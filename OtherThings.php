<?php
require('Common.php');
printHeader("Anything Else");
?>
<form action="Consents.php" method="POST">
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <h2><?php echo $translationArray[314][$LANG_ID]?></h2>
            </div>
        </div>

        <div class="row mt-3 mb-3">
            <div class="col-12">
                <textarea class="form-control" id="otherInfoText" name="otherInfoText" rows="3" maxlength="250"></textarea>
            </div>
        </div>

        <div class="row justify-content-end my-3 mx-1">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
            </div>
        </div>

    </div>

</form>
<?php printFooter(); ?>
