<?php
require('Common.php');
printHeader("Your Contact Information");
?>
<form action="DentalPlanInfo.php" method="POST">
<div class="container mt-3">

    <div class="row">
            <div class="col-auto ">
                <h4><?php echo $translationArray[76][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
    </div>
    
    <div class="row">
        <div class="col-12 col-md-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="bestContactMethod" id="HomePhone" onchange="updateTextInput()" value="HomePhone" required>
                <label class="form-check-label" for="HomePhone"><?php echo $translationArray[77][$LANG_ID]?></label>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <label for="HomePhoneNumber" class="form-label" id="HomePhoneLabel" style="display:none"><?php echo $translationArray[78][$LANG_ID]?></label>
        </div>
        <div class="col-12 col-md-4">
            <input type="text" class="form-control mb-2" id="HomePhoneNumber" name="HomePhoneNumber" style="display:none">
        </div>
    </div>
        
    <div class="row">
        <div class="col-12 col-md-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="bestContactMethod" id="CellPhone" onchange="updateTextInput()" value="CellPhone" required>
                <label class="form-check-label" for="CellPhone"><?php echo $translationArray[79][$LANG_ID]?></label>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <label for="CellPhoneNumber" class="form-label" id="CellPhoneNumberLabel" style="display:none"><?php echo $translationArray[80][$LANG_ID]?></label>
        </div>
        <div class="col-12 col-md-4">
            <input type="text" class="form-control mb-2" id="CellPhoneNumber" name="CellPhoneNumber" style="display:none">
        </div>
    </div>
        
        
    <div class="row">
        <div class="col-12 col-md-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="bestContactMethod" id="TextMe" onchange="updateTextInput()" value="TextMe" required>
                <label class="form-check-label" for="TextMe"><?php echo $translationArray[81][$LANG_ID]?></label>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <label for="TextCellPhoneNumber" class="form-label" id="TextCellPhoneNumberLabel" style="display:none"><?php echo $translationArray[80][$LANG_ID]?></label>
        </div>
        <div class="col-12 col-md-4">
            <input type="text" class="form-control mb-2" id="TextCellPhoneNumber" name="TextCellPhoneNumber" style="display:none">
        </div>
    </div>
        
    <div class="row justify-content-end my-3 mx-1">
        <div class="col-auto">
            <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
        </div>
    </div>
        
    
    <script>
        function updateTextInput() {
            var checkRadio = document.querySelector('input[name="bestContactMethod"]:checked'); 
            
            var homeLabel = document.getElementById("HomePhoneLabel");
            var homePhone = document.getElementById("HomePhoneNumber");
            var cellLabel = document.getElementById("CellPhoneNumberLabel");
            var cellNumber = document.getElementById("CellPhoneNumber");
            var textLabel =  document.getElementById("TextCellPhoneNumberLabel");
            var textNumber =  document.getElementById("TextCellPhoneNumber");
            
            if(checkRadio.value === "HomePhone") {
                homeLabel.style.display = "";
                homePhone.style.display = "";
                cellLabel.style.display = "none";
                cellNumber.style.display = "none";
                textLabel.style.display = "none";
                textNumber.style.display = "none";
            }
            
            else if(checkRadio.value === "CellPhone") {
                homeLabel.style.display = "none";
                homePhone.style.display = "none";
                cellLabel.style.display = "";
                cellNumber.style.display = "";
                textLabel.style.display = "none";
                textNumber.style.display = "none";
            
            }
            
            else if(checkRadio.value === "TextMe") {
                homeLabel.style.display = "none";
                homePhone.style.display = "none";
                cellLabel.style.display = "none";
                cellNumber.style.display = "none";
                textLabel.style.display = "";
                textNumber.style.display = "";
            }
        }
        </script>
</div>
</form>
<?php printFooter(); ?>
