<?php
require('Common.php');
printHeader("Consent to Treatment and Medical Records");
?>
<form name="sigForm" action="Done.php" method="POST">
    <div class="container mt-3" id="rootCon">
        <div class="row">
            <div class="col-12">
                <h2>Please sign the consent form</h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <h2>HIPAA CONSENT FORM</h2>
		<p><?php echo $translationArray[202][$LANG_ID]?></p>
		<h3><?php echo $translationArray[203][$LANG_ID]?>:</h3>
		<ul>
			<li><?php echo $translationArray[204][$LANG_ID]?></li>
			<li><?php echo $translationArray[205][$LANG_ID]?></li>
			<li><?php echo $translationArray[206][$LANG_ID]?></li>
			<li><?php echo $translationArray[207][$LANG_ID]?></li>
		</ul>
		<h3><?php echo $translationArray[208][$LANG_ID]?>:</h3>
		<ul>
			<li><?php echo $translationArray[209][$LANG_ID]?></li>
			<li><?php echo $translationArray[210][$LANG_ID]?></li>
		</ul>
		<p><?php echo $translationArray[211][$LANG_ID]?></p>
		<h2><?php echo $translationArray[212][$LANG_ID]?>:</h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-auto">
                <canvas style="border:1px solid #000000;" id="sigCanvas" ></canvas>
            </div>
        </div>
        
        <div class="row justify-content-between mt-3 mb-3" id="validRow" style="display:none">
            <div class="col-auto">
                <button type="button" class="btn btn-danger btn-lg" onclick="clearIt()">Clear</button>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-success btn-lg btn-block"><?php echo $translationArray[30][$LANG_ID]?></button>
            </div>
        </div>
        <div id="hideInMe"><input id="consentValues" type="hidden" name="consentValues" value=""></div>
    </div>
  
    <script>
        var dots= [];
        
        var ctx = document.getElementById("sigCanvas").getContext('2d');
        var element = document.getElementById("sigCanvas");
        var rect = element.getBoundingClientRect();
        var consentValues = document.getElementById("consentValues");
        var validRow = document.getElementById("validRow");
        var rootCon = document.getElementById("rootCon");
    
        function initSig() {
            var rootRect = rootCon.getBoundingClientRect();
            element.width = rootRect.width - 25;
            element.height = element.width /2;
        }
        
        function handleMouseMove(e) {
            //console.debug(e);
            
            if(e.buttons>0) {
                dots.push(e.offsetX);
                dots.push(e.offsetY);
                draw();
            }
        }
        
        function clearIt() {
            dots = [];
            draw();
        }
        
        
        function handleMouseRelease(e) {
            dots.push(".");
	    window.scrollTo(0,100000);
        }
        
        function handleTouchMove(e) {
            e.preventDefault();
            dots.push(e.touches[0].pageX - rect.left);
            dots.push(e.touches[0].pageY - rect.top);
            draw();
        }
        
        function handleTouchEnd(e) {
            dots.push(".");
	    window.scrollTo(0,100000);
        }
        
        function draw() {
            ctx.clearRect(0, 0, element.width, element.height);
            consentValues.value = dots.join(",");
            if(dots.length < 2) {
                validRow.style.display="none";
                return;
            }
            
            else if(dots.length < 20) {
                validRow.style.display="none";
            
            }
            else {
                validRow.style.display="";
            }
            
            
            ctx.beginPath(dots[0],dots[1]);
            for(var i=2;i<dots.length;i++) {
                if(dots[i]===".") {
                    ctx.stroke(); 
                    ctx.beginPath();
                }
                else {
                    ctx.lineTo(dots[i],dots[i+1]);
                    i++;
                
                }
            }
            ctx.stroke(); 
        }
        
        var canvas = document.getElementById('sigCanvas');
        
        canvas.addEventListener('mousemove', handleMouseMove);
        canvas.addEventListener('mouseup', handleMouseRelease);
        
        canvas.addEventListener('touchmove', handleTouchMove);
        canvas.addEventListener('touchend', handleTouchEnd);
        canvas.addEventListener('resize', initSig);
        
        initSig();
        window.requestAnimationFrame(draw);
  
    </script>
</form>
<?php printFooter(); ?>
