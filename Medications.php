<?php
require('Common.php');
printHeader("Your Medications");
?>
<form name="MedicalConditions" action="Allergies.php" method="POST">
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <h2><?php echo $translationArray[192][$LANG_ID]?></h2>
            </div>
        </div>
        
        <div class="row mt-3 mb-3">
            <div class="col-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"><?php echo $translationArray[193][$LANG_ID]?></th>
                            <th scope="col"><?php echo $translationArray[194][$LANG_ID]?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $medNameLine = $translationArray[193][$LANG_ID];
                            $medWhyLine = $translationArray[194][$LANG_ID];
                            for($i=1;$i<7;$i++) {
                                echo "<tr><td>";
                                echo "<input type=\"text\" class=\"form-control\" placeholder=\"".$medNameLine."\" name=\"medName$i\">";
                                echo "</td><td>";
                                echo "<input type=\"text\" class=\"form-control\" placeholder=\"".$medWhyLine."\" name=\"medWhy$i\">";
                                echo "</td></tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="row justify-content-end my-3 mx-1">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
            </div>
        </div>

    </div>
</form>
<?php printFooter(); ?>
