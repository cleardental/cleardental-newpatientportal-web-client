<?php
require('Common.php');
printHeader("Your Allergies");
?>
<form action="OtherThings.php" method="POST">
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <h2><?php echo $translationArray[195][$LANG_ID]?></h2>
            </div>
        </div>
        
        <div class="row mt-3 mb-3">
            <div class="col-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col"><?php echo $translationArray[196][$LANG_ID]?></th>
                            <th scope="col"><?php echo $translationArray[197][$LANG_ID]?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $whatAreAllTo = $translationArray[196][$LANG_ID];
                            $mild = $translationArray[198][$LANG_ID];
                            $moderate = $translationArray[199][$LANG_ID];
                            $severe = $translationArray[200][$LANG_ID];
                            $dontKnow = $translationArray[201][$LANG_ID];
                            for($i=1;$i<7;$i++) {
                                echo "<tr><td>";
                                echo "<input type=\"text\" class=\"form-control\" placeholder=\"".$whatAreAllTo."\" name=\"allName$i\">";
                                echo "</td><td>";
                                echo "<select class=\"form-select\" name=\"severity$i\">";
                                echo "<option value=\"Mild (Rash, itching, stomach ache)\">".$mild."</option>";
                                echo "<option value=\"Moderate (scratchy throat, watery / itchy eyes)\">".$moderate."</option>";
                                echo "<option value=\"Severe (Anaphylaxis, difficulty swallowing or breathing)\">".$severe."</option>";
                                echo "<option value=\"I don't know, somebody told me I was allergic to it\">".$dontKnow."</option>";
                                echo "</select></td></tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="row justify-content-end my-3 mx-1">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
            </div>
        </div>
    
    </div>
  
</form>
<?php printFooter(); ?>
