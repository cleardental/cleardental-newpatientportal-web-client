<?php

require('Configure.php');
require('Translation.php');

function printHeader($headerText='New Patient Form') {
    session_start();
    foreach ($_POST as $key => $value) {
    $_SESSION[$key] = $value;
    }
 echo <<<HEREDOC
 <!doctype html>
 <html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Barlow+Semi+Condensed&family=Kaushan+Script&display=swap" rel="stylesheet">

  <style>
    body {
       font-family: 'Barlow Semi Condensed', sans-serif;
    }

    h1 {
        font-family: 'Kaushan Script', cursive;
    }
    </style>

    <title>$headerText</title>
  </head>
  <body>
HEREDOC;
    global $LANG_ID;
    $LANG_ID = 0;
    if(isset($_SESSION["language"])) {
        if($_SESSION["language"] == "en") {
            $LANG_ID = 0;
        }
        else if($_SESSION["language"] == "pr") {
            $LANG_ID = 1;
        }
        else if($_SESSION["language"] == "es") {
            $LANG_ID = 2;
	}
    }
}

function printFooter() {
    echo <<<HEREDOC
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
HEREDOC;
}

function printMonths() {
    $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    for($i=1;$i<13;$i++) {
        echo "<option value=\"$i\">".$months[$i-1]."</option>";
    }
}

function printDays() {
    for($i=1;$i<33;$i++) {
        echo "<option value=\"$i\">$i</option>";
    }
}

function printYears() {
    $startYear = (int)date("Y");
    for($i = $startYear; $i > ($startYear - 128); $i--) {
        echo "<option value=\"$i\">$i</option>";
    }
}

function printStates() {
    echo <<<HEREDOC
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA" selected>Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
HEREDOC;
}

function cdTr($input) {
    if($LANG_ID == 0) {
        return $input;
    }

    $trArrayLength = count($translationArray);

    for($i=0;$i<$trArrayLength;$i++) {
        if($translationArray[$i][0] == $input) {
            return $translationArray[$i][$LANG_ID];
        }
    }
    return $input;
}


?>
