<?php
require('Common.php');
printHeader();
?>
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-auto ">
                <img width=256 height=256 src="practiceLogo.png">
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-auto ">
                <h1><?php echo $translationArray[2][$LANG_ID]?></h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col-auto ">
                <h2><?php echo $translationArray[3][$LANG_ID]?></h2>
            </div>
        </div>
        
        
        <div class="row mt-3">
            <div class="col-auto ">
                <h3><?php echo $translationArray[4][$LANG_ID]?></h3>
            </div>
        </div>
        
        <div class="row justify-content-between mt-3">
            <div class="col-auto">
                <a class="btn btn-danger btn-lg mb-3" href="PainPage.php" role="button"><?php echo $translationArray[5][$LANG_ID]?></a>
            </div>
            <div class="col-auto">
                <a class="btn btn-primary btn-lg mb-3" href="NoPainPage.php" role="button"><?php echo $translationArray[6][$LANG_ID]?></a>
            </div>
        </div>
    </div>
<?php
printFooter();
?>
