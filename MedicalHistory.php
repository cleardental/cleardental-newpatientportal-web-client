<?php
require('Common.php');
printHeader("Your Medical History");
?>
<form action="MedicalConditions.php" method="POST">
  <div class="container mt-3">
    <div class="row">
        <div class="col-auto">
            <h2><?php echo $translationArray[116][$LANG_ID]?></h2>
        </div>
    </div>
    
    
    <div class="row align-items-center" <?php if($_SESSION["sex"] != "Female"){echo "style=\"display:none\"";} ?> >
        <div class="col-12 col-md-6 mb-2">
            <h4><?php echo $translationArray[117][$LANG_ID]?></h4>
        </div>
        <div class="col-12 col-md-4 mb-2">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="breastfeeding" id="yesBreastfeeding" value="yesBreastfeeding">
                <label class="form-check-label" for="yesBreastfeeding"><?php echo $translationArray[98][$LANG_ID]?></label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="breastfeeding" id="noBreastfeeding" value="noBreastfeeding" checked>
                <label class="form-check-label" for="noBreastfeeding"><?php echo $translationArray[99][$LANG_ID]?></label>
            </div>
        </div>
    </div>
    
    
    <div class="row align-items-center" <?php if($_SESSION["sex"] != "Female"){echo "style=\"display:none\"";} ?>>
        <div class="col-12 col-md-6 mb-2">
            <h4><?php echo $translationArray[118][$LANG_ID]?></h4>
        </div>
        <div class="col-12 col-md-4 mb-2">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="isPregnant" id="yesPregnant" value="yesPregnant"  onchange="updatePregnant()">
                <label class="form-check-label" for="yesPregnant"><?php echo $translationArray[98][$LANG_ID]?></label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="isPregnant" id="noPregnant" value="noPregnant" onchange="updatePregnant()" checked>
                <label class="form-check-label" for="noPregnant"><?php echo $translationArray[99][$LANG_ID]?></label>
            </div>
        </div>
    </div>
    
    <script>
    function updatePregnant() {
        var isPregnant = document.querySelector('input[name="isPregnant"]:checked'); 
        var pregRow = document.getElementById("pregRow");
        if(isPregnant.value === "yesPregnant") {
            pregRow.style.display = "";
        }
        else {
            pregRow.style.display = "none";
        }
    }
    </script>
    
    <div class="row" id="pregRow" style="display:none">
        <div class="col-12 col-md-6 mb-3" >
            <label for="pregDueDate" class="form-label">Due Date</label>
            <input type="date" class="form-control" id="pregDueDate" name="pregDueDate">
        </div>
        <div class="col-12 col-md-6 mb-3" >
            <label for="pregOBGYN" class="form-label">Name of your OB/GYN</label>
            <input type="text" class="form-control" id="pregOBGYN" name="pregOBGYN">
        </div>
    </div>
    
    
    <div class="row align-items-center">
        <div class="col-12 col-md-6 mb-2">
            <h4><?php echo $translationArray[119][$LANG_ID]?></h4>
        </div>
        <div class="col-12 col-md-4 mb-2">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="takeAntibio" id="yesAntibio" value="yesAntibio"  onchange="updateAntibio()">
                <label class="form-check-label" for="yesAntibio"><?php echo $translationArray[98][$LANG_ID]?></label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="takeAntibio" id="noAntibio" value="noAntibio" onchange="updateAntibio()" checked>
                <label class="form-check-label" for="noAntibio"><?php echo $translationArray[99][$LANG_ID]?></label>
            </div>
        </div>
    </div>
    
    <script>
    function updateAntibio() {
        var takeAntibio = document.querySelector('input[name="takeAntibio"]:checked'); 
        var antiBioRow = document.getElementById("antiBioRow");
        if(takeAntibio.value === "yesAntibio") {
            antiBioRow.style.display = "";
        }
        else {
            antiBioRow.style.display = "none";
        }
    }
    </script>
    
    <div class="row" id="antiBioRow" style="display:none">
        <div class="col-12 col-md-6 mb-3" >
            <label for="antbioWhy" class="form-label">For what reason?</label>
            <select class="form-select" id="antbioWhy" name="antbioWhy">
                <option>Because of a heart condition</option>
                <option>Because of a joint replacement</option>
                <option>I don't know, a doctor just told me to take it</option>
            </select>
        </div>
        <div class="col-12 col-md-6 mb-3" >
            <label for="antibioDoc" class="form-label">Name of doctor recommending you take antibiotics</label>
            <input type="text" class="form-control" id="antibioDoc" name="antibioDoc">
        </div>
    </div>
    
    
    <div class="row align-items-center">
        <div class="col-12 col-md-6 mb-2">
            <h4><?php echo $translationArray[120][$LANG_ID]?></h4>
        </div>
        <div class="col-12 col-md-4 mb-2">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="hxCancer" id="yesHxCancer" value="yesHxCancer"  onchange="updateCancer()">
                <label class="form-check-label" for="yesHxCancer"><?php echo $translationArray[98][$LANG_ID]?></label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="hxCancer" id="noHxCancer" value="noHxCancer" onchange="updateCancer()" checked>
                <label class="form-check-label" for="noHxCancer"><?php echo $translationArray[99][$LANG_ID]?></label>
            </div>
        </div>
    </div>
    
    <script>
    function updateCancer() {
        var hxCancer = document.querySelector('input[name="hxCancer"]:checked'); 
        var cancerRow = document.getElementById("cancerRow");
        if(hxCancer.value === "yesHxCancer") {
            cancerRow.style.display = "";
        }
        else {
            cancerRow.style.display = "none";
        }
    }
    </script>
    
    <div class="row" id="cancerRow" style="display:none">
        <div class="col-12 col-md-6 mb-3" >
            <label for="antbioWhy" class="form-label">Where/What kind?</label>
            <input type="text" class="form-control" id="antbioWhy" name="antbioWhy">
        </div>
        <div class="col-12 col-md-6 mb-3" >
            <label  class="form-label">Did you ever get head or neck radiation?</label>
            <div class="col-auto">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="hxRadiation" id="yesRadiation" value="yesRadiation">
                    <label class="form-check-label" for="yesRadiation">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="hxRadiation" id="noRadiation" value="noRadiation" checked>
                    <label class="form-check-label" for="noRadiation">No</label>
                </div> <!--End of inline check-->
            </div> <!--End of column-->
        </div> <!--End of head/neck col-->
    </div> 
    
    <div class="row justify-content-end my-3 mx-1">
        <div class="col-auto">
            <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
        </div>
    </div>
  </div>
</form>
<?php printFooter(); ?>
