<?php
require('Common.php');
printHeader("About your pain");
$_SESSION["patientInPain"] = true;
?>
<form action="BasicPersonalInfoPage.php" method="POST">
  <div class="container mt-3">
  
    <div class="row justify-content-center">
            <div class="col-auto">
                <h2><?php echo $translationArray[7][$LANG_ID]?></h2>
                <h2 class="text-danger"><?php echo $translationArray[8][$LANG_ID]?></h2>
            </div>
    </div>
    
    <div class="row">
            <div class="col-auto ">
                <h3><?php echo $translationArray[9][$LANG_ID]?></h3>
            </div>
    </div>
    
    <div class="row mt-4">
        <div class="col-12 col-md-6">
            <h4><?php echo $translationArray[10][$LANG_ID]?><span class="text-danger">*</span></h4>
        </div>
        <div class="col-12 col-md-6">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="painLength" id="lessThan1Day" value="24h" required>
                <label class="form-check-label" for="lessThan1Day"><?php echo $translationArray[11][$LANG_ID]?></label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="painLength" id="someTimeAgo" value="2w" required>
                <label class="form-check-label" for="someTimeAgo"><?php echo $translationArray[12][$LANG_ID]?></label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="painLength" id="forSomeWhile" value="6mon" required>
                <label class="form-check-label" for="forSomeWhile"><?php echo $translationArray[13][$LANG_ID]?></label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="painLength" id="longTime" value="longTime" required>
                <label class="form-check-label" for="longTime"><?php echo $translationArray[14][$LANG_ID]?></label>
            </div>
        </div>
    </div> <!--End Row-->
    
    
    <div class="row mt-4">
        <div class="col-12 col-md-6">
            <h4><?php echo $translationArray[15][$LANG_ID]?><span class="text-danger">*</span></h4>
        </div>
        
        <div class="col-12 col-md-6">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="whereFeel" id="ur" value="ur" required>
                <label class="form-check-label" for="ur"><?php echo $translationArray[16][$LANG_ID]?></label>
            </div>
            
            <div class="form-check">
                <input class="form-check-input" type="radio" name="whereFeel" id="uc" value="uc" required>
                <label class="form-check-label" for="uc"><?php echo $translationArray[17][$LANG_ID]?></label>
            </div>

            <div class="form-check">
                <input class="form-check-input" type="radio" name="whereFeel" id="ul" value="ul" required>
                <label class="form-check-label" for="ul"><?php echo $translationArray[18][$LANG_ID]?></label>
            </div>

            <div class="form-check">
                <input class="form-check-input" type="radio" name="whereFeel" id="lr" value="lr" required>
                <label class="form-check-label" for="lr"><?php echo $translationArray[19][$LANG_ID]?></label>
            </div>

            <div class="form-check">
                <input class="form-check-input" type="radio" name="whereFeel" id="lc" value="lc" required>
                <label class="form-check-label" for="lc"><?php echo $translationArray[20][$LANG_ID]?></label>
            </div>

            <div class="form-check">
                <input class="form-check-input" type="radio" name="whereFeel" id="ll" value="ll" required>
                <label class="form-check-label" for="ll"><?php echo $translationArray[21][$LANG_ID]?></label>
            </div>
        </div>
    </div> <!--End Row-->
    
    
    <div class="row mt-4">
        <div class="col-12 col-md-6">
            <h4><?php echo $translationArray[22][$LANG_ID]?><span class="text-danger">*</span></h4>
        </div>
        
        <div class="col-12 col-md-6">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="wantSave" id="saveIt" value="saveIt" onchange="myFunction()" required>
                <label class="form-check-label" for="saveIt" ><?php echo $translationArray[23][$LANG_ID]?></label>
            </div>

            <div class="form-check">
                <input class="form-check-input" type="radio" name="wantSave" id="leaveIt" value="leaveIt" onchange="myFunction()" required>
                <label class="form-check-label" for="leaveIt"><?php echo $translationArray[24][$LANG_ID]?></label>
            </div>
            
            <div class="row">
                    <div class="col-auto">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="wantSave" id="depends" value="depends" onchange="myFunction()" required>
                            <label class="form-check-label" for="depends"><?php echo $translationArray[25][$LANG_ID]?></label>
                        </div>
                    </div>
                    <div class="col-auto">
                        <label for="maxSpendSave" class="form-label" id="maxSpendSaveLabel" style="display:none"><?php echo $translationArray[26][$LANG_ID]?></label>
                    </div>
                    <div class="col-auto">
                        <input type="text" class="form-control" name="dependsPrice" id="maxSpendSave" style="display:none">
                    </div>
            </div>
        </div>
    </div> <!--End Row-->
    
    <div class="row mt-4">
        <div class="col-12 col-md-6">
            <h4><?php echo $translationArray[27][$LANG_ID]?><span class="text-danger">*</span></h4>
        </div>
        
        <div class="col-12 col-md-6">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="howSoon" id="today" value="today" required>
                <label class="form-check-label" for="today" ><?php echo $translationArray[28][$LANG_ID]?></label>
            </div>

            <div class="form-check">
                <input class="form-check-input" type="radio" name="howSoon" id="later" value="later" required>
                <label class="form-check-label" for="later"><?php echo $translationArray[29][$LANG_ID]?></label>
            </div>
            
        </div>
    </div> <!--End Row-->
    
    <div class="row justify-content-end my-3 mx-1">
        <div class="col-auto">
            <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
        </div>
    </div>
    
    <script>
        function myFunction() {
            var setMe = document.getElementById("maxSpendSave");
            var setMe2 = document.getElementById("maxSpendSaveLabel");
            var checkRadio = document.querySelector('input[name="wantSave"]:checked'); 
            if(checkRadio.value === "depends") {
                setMe.style.display = "";
                setMe2.style.display = "";
            }
            else {
                setMe.style.display = "none";
                setMe2.style.display = "none";
            }
        }    
    </script>
  </div>
</form>
<?php printFooter(); ?>
