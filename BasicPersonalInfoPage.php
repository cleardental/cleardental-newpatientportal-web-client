<?php
require('Common.php');
printHeader("Your Basic Information");
if($_SESSION["patientInPain"]) {
    echo "<form action=\"QuickContact.php\" method=\"POST\">";
}
else {
    echo "<form action=\"DetailedContact.php\" method=\"POST\">";
}
?>
<div class="container mt-3">
    <h2>About You</h2>

    <div class="row my-4">
        <div class="col-12 col-md-4">
            <div class="form-floating my-1">
                <input type="text" class="form-control" id="firstName" placeholder="" name="firstName" value="" required>
                <label for="firstName" class="form-label"><?php echo $translationArray[31][$LANG_ID]?><span class="text-danger">*</span></label>
            </div>
            <div class="invalid-feedback"><?php echo $translationArray[32][$LANG_ID]?></div>
        </div>
        <div class="col-12 col-md-4">
            <div class="form-floating my-1">
                <input type="text" class="form-control" id="middleName" name="middleName" placeholder="" value="">
                <label for="middleName" class="form-label"><?php echo $translationArray[33][$LANG_ID]?></label>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="form-floating my-1">
                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="" value="" required>
                <label for="lastName" class="form-label"><?php echo $translationArray[34][$LANG_ID]?><span class="text-danger">*</span></label>
            </div>
            <div class="invalid-feedback"><?php echo $translationArray[35][$LANG_ID]?></div>
        </div>
        <div class="col-12">
            <div class="form-floating my-1">
                <input type="text" class="form-control" id="preferredName" name="preferredName" placeholder="" value="">
                <label for="preferredName" class="form-label"><?php echo $translationArray[36][$LANG_ID]?></label>
            </div>
        </div>
    </div>

    <div class="row my-4">
        <div class="col-md-6 my-1">
            <label for="sex" class="form-label h3"><?php echo $translationArray[37][$LANG_ID]?><span class="text-danger">*</span></label>
            <div class="btn-group btn-group-lg mx-2" role="group" aria-label="Basic radio toggle button group">
                <input type="radio" class="btn-check" name="sex" autocomplete="off" id="maleRadio" value="Male"  required>
                <label class="btn btn-outline-primary" for="maleRadio"><?php echo $translationArray[38][$LANG_ID]?></label>

                <input type="radio" class="btn-check" name="sex" autocomplete="off" id="femaleRadio" value="Female" required>
                <label class="btn btn-outline-primary" for="femaleRadio"><?php echo $translationArray[39][$LANG_ID]?></label>

                <input type="radio" class="btn-check" name="sex" autocomplete="off" id="otherSexRadio" value="Other" required>
                <label class="btn btn-outline-primary" for="otherSexRadio"><?php echo $translationArray[40][$LANG_ID]?></label>
            </div>
        </div>
        <div class="col-md-6 my-1" >
            <div class="form-floating">
                <input type="text" class="form-control" id="gender" name="gender" placeholder="" value="">
                <label for="gender" class="form-label"><?php echo $translationArray[41][$LANG_ID]?></label>
            </div>
        </div>
    </div>

    <div class="row my-4">
        <div class="col-12 col-md-3" >
            <label for="dob" class="form-label h3"><?php echo $translationArray[42][$LANG_ID]?><span class="text-danger">*</span></label>
            <!--<input type="date" class="form-control" id="dob" name="dob" required> -->
        </div>
        <div class="col-4 col-md-3" >
            <select name="dob-month" id="dob-month" class="form-select" required>
                    <option value="">Month</option>
                    <?php printMonths(); ?>
            </select>
        </div>
        <div class="col-4 col-md-3" >
            <select name="dob-day" id="dob-day" class="form-select" required>
                <option value="">Day</option>
                <?php printDays(); ?>
            </select>
        </div>
        <div class="col-4 col-md-3" >
            <select name="dob-year" id="dob-year" class="form-select" required>
                <option value="">Year</option>
                <?php printYears(); ?>
            </select>
        </div>
    </div>

    <div class="row my-4">
        <div class="col-md-6 my-1">
            <label for="Race" class="form-label h3"><?php echo $translationArray[43][$LANG_ID]?></label>
            <select class="form-select" id="Race" name="Race">
            <option value="I choose not to answer"><?php echo $translationArray[44][$LANG_ID]?></option>
            <option value="American Indian or Alaska native"><?php echo $translationArray[45][$LANG_ID]?></option>
            <option value="Asian"><?php echo $translationArray[46][$LANG_ID]?></option>
            <option value="Black or African American"><?php echo $translationArray[47][$LANG_ID]?></option>
            <option value="Polynesian"><?php echo $translationArray[48][$LANG_ID]?></option>
            <option value="White"><?php echo $translationArray[49][$LANG_ID]?></option>
            <option value="Other"><?php echo $translationArray[50][$LANG_ID]?></option>
            </select>
        </div>
        <div class="col-md-6 my-1">
            <label for="Ethnicity" class="form-label h3"><?php echo $translationArray[51][$LANG_ID]?></label>
            <select class="form-select" id="Ethnicity" name="Ethnicity">
            <option value="I choose not to answer"><?php echo $translationArray[44][$LANG_ID]?></option>
            <option value="Hispanic Or Latino"><?php echo $translationArray[52][$LANG_ID]?></option>
            <option value="Not Hispanic or Latino"><?php echo $translationArray[53][$LANG_ID]?></option>
            </select>
        </div>
    </div>

    <div class="row my-4">
        <div class="col-md-6">
            <label for="RefSource" class="form-label h3"><?php echo $translationArray[54][$LANG_ID]?></label>
            <select class="form-select" id="RefSource" name="RefSource" onchange="updateRefName()">
                <option value="Google Search"><?php echo $translationArray[55][$LANG_ID]?></option>
                <option value="Google Maps"><?php echo $translationArray[56][$LANG_ID]?></option>
                <option value="Smash Bros. Challenge"><?php echo $translationArray[57][$LANG_ID]?></option>
                <option value="Facebook"><?php echo $translationArray[58][$LANG_ID]?></option>
                <option value="YouTube"><?php echo $translationArray[59][$LANG_ID]?></option>
                <option value="Reddit"><?php echo $translationArray[60][$LANG_ID]?></option>
                <option value="Bing"><?php echo $translationArray[61][$LANG_ID]?></option>
                <option value="I saw the sign"><?php echo $translationArray[62][$LANG_ID]?></option>
                <option value="Yelp"><?php echo $translationArray[63][$LANG_ID]?></option>
                <option value="Postcard"><?php echo $translationArray[64][$LANG_ID]?></option>
                <option value="Town of Ashland E-mail or Website"><?php echo $translationArray[65][$LANG_ID]?></option>
                <option value="From a friend"><?php echo $translationArray[66][$LANG_ID]?></option>
                <option value="Dr. Sal">Dr. Sal</option>
            </select>
        </div>
        <div class="col-md-6" id="RefNameDiv" style="display:none" >
            <div class="form-floating mt-3">
                <input type="text" class="form-control" id="RefName" name="RefName">
                <label for="RefName" class="form-label"><?php echo $translationArray[67][$LANG_ID]?></label>
            </div>
        </div>
    </div>
        
    <div class="row my-4">
        <div class="col-md-6">
            <div class="form-floating my-1">
                <input type="text" class="form-control" id="addr1" name="addr1" placeholder="" value="" required>
                <label for="addr1" class="form-label"><?php echo $translationArray[68][$LANG_ID]?><span class="text-danger">*</span></label>
            </div>
            <div class="invalid-feedback"><?php echo $translationArray[69][$LANG_ID]?></div>
        </div>
        <div class="col-md-6">
            <div class="form-floating my-1">
                <input type="text" class="form-control" id="addr2" name="addr2" placeholder="" value="">
                <label for="addr2" class="form-label"><?php echo $translationArray[70][$LANG_ID]?></label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-floating my-1">
                <input type="text" class="form-control" id="HomeCity" name="HomeCity"  placeholder="" value="" required>
                <label for="HomeCity" class="form-label"><?php echo $translationArray[71][$LANG_ID]?><span class="text-danger">*</span></label>
            </div>
            <div class="invalid-feedback"><?php echo $translationArray[72][$LANG_ID]?></div>
        </div>
        <div class="col-md-4">
            <div class="form-floating my-1">
                <input type="text" class="form-control" id="HomeZip" name="HomeZip" placeholder="" value="" required >
                <label for="HomeZip" class="form-label"><?php echo $translationArray[73][$LANG_ID]?><span class="text-danger">*</span></label>
            </div>
        </div>
        <div class="col-md-4 align-self-center my-1">
            <select class="form-select form-select-lg" id="HomeState" name="HomeState" required >
                <?php printStates(); ?>
            </select>
        </div>
    </div>

    <div class="row justify-content-end my-3 mx-1">
        <div class="col-auto">
            <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
        </div>
    </div>
      
    <script>
    function updateRefName() {
        var setMe = document.getElementById("RefNameDiv");
        var sourceMe = document.getElementById("RefSource");
        if(sourceMe.value === "From a friend") {
            setMe.style="";
        }
        else {
            setMe.style.display = "none";
        }

    }
    </script>
</div>
</form>
<?php
printFooter();
?>
