<?php
$translationArray = array (
  1 => 
  array (
    0 => 'English',
    1 => 'Portuguese',
    2 => 'Spanish',
  ),
  2 => 
  array (
    0 => 'Thank you for your interest in '.$PRACTICE_NAME,
    1 => 'Obrigado pelo seu interesse na '.$PRACTICE_NAME,
    2 => 'Gracias por su interesse en '.$PRACTICE_NAME,
  ),
  3 => 
  array (
    0 => 'To help you on your first visit, we will need to ask you a few questions. If you have any issues along the way, feel free to contact us at '.$PRACTICE_PHONE_NUMBER.'. If nobody picks up the phone, please email '.$PRACTICE_EMAIL.' and let us know you want to come in. Please note that anything with a red asterisk (<span class="text-danger">*</span>) is required.',
    1 => 'Para ajudá-lo em sua primeira visita, precisaremos fazer algumas perguntas. Se você tiver algum problema ao longo do caminho, não hesite em entrar em contato conosco pelo telefone '.$PRACTICE_PHONE_NUMBER.'. Se ninguém atender o telefone, envie um e-mail para '.$PRACTICE_EMAIL.' e nos avise que deseja entrar. Por favor, observe que qualquer item com um asterisco vermelho (<span class="text-danger">*</span>) é necessária. ',
    2 => 'Para ayudarle en su primera visita, tendremos que hacerle algunas preguntas. Si tiene algún problema a lo largo de las perguntas, no dude en contactarnos en '.$PRACTICE_PHONE_NUMBER.'. Si nadie contesta el teléfono, envíe un correo electrónico a '.$PRACTICE_EMAIL.' y dejemos saber que desea hacer. Tenga en cuenta que se requiere cualquier cosa con un asterisco rojo (<span class="text-danger">*</span>).'),
  4 => 
  array (
    0 => 'Are you in pain?',
    1 => 'Você esta com dor?',
    2 => '¿Estás con dolor?',
  ),
  5 => 
  array (
    0 => 'Yes, I am in Pain!',
    1 => 'Sim, estou com dor',
    2 => 'Si, tengo dolor',
  ),
  6 => 
  array (
    0 => 'No, I am not in any pain',
    1 => 'Não, eu não estou com dor',
    2 => 'No tengo ningun dolor.',
  ),
  7 => 
  array (
    0 => 'Sorry to hear about your pain 😥.',
    1 => 'Lamento ouvir sobre sua dor 😥.',
    2 => 'Lamento escuchar sobre su dolor',
  ),
  8 => 
  array (
    0 => 'If this is an emergency that can not wait, please stop and call 911.',
    1 => 'Se esta for uma emergência que não pode esperar, pare e ligue para o 911. ',
    2 => 'Si se trata de una emergencia que no puede esperar, deténgase y llame al 911.',
  ),
  9 => 
  array (
    0 => 'To help you further, we need to ask you a few questions about your pain.',
    1 => 'Para ajudá-lo ainda mais, precisamos fazer algumas perguntas sobre sua dor.',
    2 => 'Para ayudarlo más, necesitamos hacerle algunas preguntas sobre su dolor.',
  ),
  10 => 
  array (
    0 => 'How long ago did you start getting pain?',
    1 => 'Há quanto tempo você começou a sentir dor?',
    2 => '¿Hace cuánto empezaste a sentir dolor?',
  ),
  11 => 
  array (
    0 => 'Recently (less than 24 hours ago)',
    1 => 'Recentemente (menos de 24 horas atrás)',
    2 => 'Recientemente (hace menos de 24 horas)',
  ),
  12 => 
  array (
    0 => 'Some time ago (between a day and 2 weeks ago)',
    1 => 'Algum tempo atrás (entre um dia e 2 semanas atrás)',
    2 => 'Hace algún tiempo (entre un día y 2 semanas)',
  ),
  13 => 
  array (
    0 => 'I\'ve had it for a while now (2 weeks to 6 months)',
    1 => 'Estou com dor há algum tempo (2 semanas a 6 meses)',
    2 => 'Lo he tenido por un tiempo ahora (2 semanas a 6 meses)',
  ),
  14 => 
  array (
    0 => 'For a long time (more than 6 months)',
    1 => 'Por muito tempo (mais de 6 meses)',
    2 => 'Por mucho tiempo (más de 6 meses)',
  ),
  15 => 
  array (
    0 => 'Where do you feel it (the most)?',
    1 => 'Onde você sente a dor?',
    2 => 'Dónde lo sientes más',
  ),
  16 => 
  array (
    0 => 'Upper Back Right',
    1 => 'Em cima, no lado direito Superior',
    2 => 'Arriba, en la parte superior derecha',
  ),
  17 => 
  array (
    0 => 'Upper Front',
    1 => 'Em cima, Na frente, Superior ',
    2 => 'Encima, Delante, Superior',
  ),
  18 => 
  array (
    0 => 'Upper Back Left',
    1 => 'Atrás, no lado esquerdo Superior',
    2 => 'Atrás, lado superior izquierdo',
  ),
  19 => 
  array (
    0 => 'Lower Back Right',
    1 => 'Atrás, no lado direito Inferior',
    2 => 'Atrás, lado inferior derecho',
  ),
  20 => 
  array (
    0 => 'Lower Font',
    1 => 'Em baixo, na frente ',
    2 => 'Abajo, delante',
  ),
  21 => 
  array (
    0 => 'Lower Back Left',
    1 => 'Em baixo, na frente, inferior esquerda',
    2 => 'Abajo, adelante, izquierda',
  ),
  22 => 
  array (
    0 => 'Assuming that the tooth can be saved, would you be interested in saving the tooth?',
    1 => 'Supondo que o dente possa ser salvo, você estaria interessado em salvá-lo? ',
    2 => 'Suponiendo que el diente se pueda salvar, ¿le interesaría salvar el diente?',
  ),
  23 => 
  array (
    0 => 'Yes, I wish to save the tooth',
    1 => 'Sim, eu quero salvar o dente',
    2 => 'Sí, deseo salvar el diente.',
  ),
  24 => 
  array (
    0 => 'No, I have no interest in saving the tooth',
    1 => 'Não, não tenho interesse em salvar o dente',
    2 => 'No, no deseo en salvar el diente',
  ),
  25 => 
  array (
    0 => 'Depends on how much it would cost me',
    1 => 'Depende de quanto isso me custaria',
    2 => 'Depende de cuanto me costaría',
  ),
  26 => 
  array (
    0 => 'How much are you willing to spend?',
    1 => 'Quanto você está disposto a gastar?',
    2 => '¿Cuánto estás dispuesto a gastar?',
  ),
  27 => 
  array (
    0 => 'How soon can you come in?',
    1 => 'Qual a data mais próxima que você pode vir?',
    2 => '¿Qué tan pronto puedes venir?',
  ),
  28 => 
  array (
    0 => 'I can come in today',
    1 => 'Posso vir hoje',
    2 => 'Puedo ir hoy',
  ),
  29 => 
  array (
    0 => 'I can\'t make it today, but I want to be seen soon',
    1 => 'Não posso ir hoje, mas quero uma consulta em breve',
    2 => 'No puedo ir hoy, pero quiero que me vean pronto.',
  ),
  30 => 
  array (
    0 => 'Next',
    1 => 'Próximo',
    2 => 'Próximo',
  ),
  31 => 
  array (
    0 => 'First Name',
    1 => 'Primeiro nome',
    2 => 'Primer nombre',
  ),
  32 => 
  array (
    0 => 'Valid First Name is required.',
    1 => 'Primeiro nome é obrigatório.',
    2 => 'Primer nombre es obligatorio',
  ),
  33 => 
  array (
    0 => 'Middle name or initial',
    1 => 'Nome do meio ou inicial',
    2 => 'Nome del medio o inicial',
  ),
  34 => 
  array (
    0 => 'Last Name',
    1 => 'Sobrenome nome',
    2 => 'Apellido',
  ),
  35 => 
  array (
    0 => 'Valid Last Name is required.',
    1 => 'É necessário um sobrenome válido.',
    2 => 'Apellido es obligatorio',
  ),
  36 => 
  array (
    0 => 'Do you have a nickname you would like to be called?',
    1 => 'Você tem um apelido pelo qual gostaria de ser chamado?',
    2 => '¿Tienes un apodo que te gustaría que te llamen?',
  ),
  37 => 
  array (
    0 => 'Sex',
    1 => 'Sexo',
    2 => 'Sexo',
  ),
  38 => 
  array (
    0 => 'Male',
    1 => 'Masculino',
    2 => 'Masculino',
  ),
  39 => 
  array (
    0 => 'Female',
    1 => 'Feminino',
    2 => 'Femenino',
  ),
  40 => 
  array (
    0 => 'Other',
    1 => 'Outro',
    2 => 'Otro',
  ),
  41 => 
  array (
    0 => 'Gender (if different from sex)',
    1 => 'Gênero (se for diferente de sexo)',
    2 => 'Género (si es diferente del sexo)',
  ),
  42 => 
  array (
    0 => 'Date of Birth',
    1 => 'Data de nascimento',
    2 => 'Fecha de nacimiento',
  ),
  43 => 
  array (
    0 => 'Race',
    1 => 'Raça',
    2 => 'Raza',
  ),
  44 => 
  array (
    0 => 'I choose not to answer',
    1 => 'Eu escolho não responder',
    2 => 'Elijo no contestar',
  ),
  45 => 
  array (
    0 => 'American Indian or Alaska native',
    1 => 'Índio americano ou nativo do Alasca',
    2 => 'Indio americano o nativa de Alaska',
  ),
  46 => 
  array (
    0 => 'Asian',
    1 => 'Asiática',
    2 => 'Asiática',
  ),
  47 => 
  array (
    0 => 'Black or African American',
    1 => 'Negro ou afro-americano',
    2 => 'Negro o afroamericano',
  ),
  48 => 
  array (
    0 => 'Polynesian',
    1 => 'Polinésia',
    2 => 'Polinésia',
  ),
  49 => 
  array (
    0 => 'White',
    1 => 'Branca',
    2 => 'Blanco',
  ),
  50 => 
  array (
    0 => 'Other',
    1 => 'Outra',
    2 => 'Otra',
  ),
  51 => 
  array (
    0 => 'Ethnicity',
    1 => 'Etnia',
    2 => 'Etnicidad',
  ),
  52 => 
  array (
    0 => 'Hispanic Or Latino',
    1 => 'Hispânico ou Latino',
    2 => 'Hispano o Latino',
  ),
  53 => 
  array (
    0 => 'Not Hispanic or Latino',
    1 => 'Não hispânico ou latino',
    2 => 'No Hispano o Latino',
  ),
  54 => 
  array (
    0 => 'How did you hear about us?',
    1 => 'Como você ficou sabendo sobre nós?',
    2 => 'Como supiste de nosotros',
  ),
  55 => 
  array (
    0 => 'Google Search',
    1 => 'Pesquisa do Google',
    2 => 'Buscando en google ',
  ),
  56 => 
  array (
    0 => 'Google Maps',
    1 => 'Google Maps',
    2 => 'Google Maps',
  ),
  57 => 
  array (
    0 => 'Smash Bros. Challenge',
    1 => 'Smash Bros. Challenge',
    2 => 'Smash Bros. Challenge',
  ),
  58 => 
  array (
    0 => 'Facebook',
    1 => 'Facebook',
    2 => 'Facebook',
  ),
  59 => 
  array (
    0 => 'YouTube',
    1 => 'YouTube',
    2 => 'YouTube',
  ),
  60 => 
  array (
    0 => 'Reddit',
    1 => 'Reddit',
    2 => 'Reddit',
  ),
  61 => 
  array (
    0 => 'Bing',
    1 => 'Bing',
    2 => 'Bing',
  ),
  62 => 
  array (
    0 => 'I saw the sign',
    1 => 'Eu vi o cartaz',
    2 => 'Lo vi en un anuncio ',
  ),
  63 => 
  array (
    0 => 'Yelp',
    1 => 'Yelp',
    2 => 'Yelp',
  ),
  64 => 
  array (
    0 => 'Postcard',
    1 => 'Cartão postal',
    2 => 'Tarjeta postal ',
  ),
  65 => 
  array (
    0 => 'Town of Ashland E-mail or Website',
    1 => 'E-mail ou site da cidade de Ashland',
    2 => 'Ciudad  de ashland correo electrónico o sitio web',
  ),
  66 => 
  array (
    0 => 'From a friend',
    1 => 'Por um amigo',
    2 => 'Por un amigo ',
  ),
  67 => 
  array (
    0 => 'May we ask who?',
    1 => 'Podemos perguntar quem?',
    2 => '¿Podemos preguntar quién',
  ),
  68 => 
  array (
    0 => 'Home Street Address',
    1 => 'Endereço residencial',
    2 => 'Direccion de su casa ',
  ),
  69 => 
  array (
    0 => 'Valid address name is required.',
    1 => 'É necessário um endereço válido.',
    2 => 'Se requiere una dirección válida',
  ),
  70 => 
  array (
    0 => 'Home Street Address (Line 2)',
    1 => 'Endereço residencial (linha 2)',
    2 => 'Direccion de su casa (linea 2)',
  ),
  71 => 
  array (
    0 => 'Home City',
    1 => 'Cidade',
    2 => 'Ciudad',
  ),
  72 => 
  array (
    0 => 'Valid home city is required.',
    1 => 'É necessária uma cidade natal válida.',
    2 => 'Se requiere una ciudad válida',
  ),
  73 => 
  array (
    0 => 'Zip Code',
    1 => 'Código postal',
    2 => 'Código postal',
  ),
  74 => 
  array (
    0 => 'State',
    1 => 'Estado',
    2 => 'Estado',
  ),
  75 => 
  array (
    0 => 'Contact Information',
    1 => 'Informações de contato',
    2 => 'Información del contacto',
  ),
  76 => 
  array (
    0 => 'What is the best way for us to contact you?',
    1 => 'Qual é a melhor maneira de entrarmos em contato com você?',
    2 => 'Como prefieres ser contactado?',
  ),
  77 => 
  array (
    0 => 'Call Home Phone',
    1 => 'Ligue para o telefone residencial',
    2 => 'Llamarme por telefono residencial',
  ),
  78 => 
  array (
    0 => 'Home Phone Number',
    1 => 'Número de telefone residencial',
    2 => 'Número de telefono residencial',
  ),
  79 => 
  array (
    0 => 'Call Cell Phone',
    1 => 'Ligue para o celular',
    2 => 'Llamarme por celular',
  ),
  80 => 
  array (
    0 => 'Cell Phone Number',
    1 => 'Número de telefone celular',
    2 => '',
  ),
  81 => 
  array (
    0 => 'Text Me',
    1 => 'Envie-me de texto',
    2 => 'Número de telefono celular',
  ),
  82 => 
  array (
    0 => 'Contact Me Via WhatsApp',
    1 => 'Contate-me via WhatsApp',
    2 => 'Contáctame a través de WhatsApp',
  ),
  83 => 
  array (
    0 => 'Dental Plan Information',
    1 => 'Informações do plano dentário',
    2 => 'Información del plan dental',
  ),
  84 => 
  array (
    0 => 'Do you have a Dental Plan or Insurance?',
    1 => 'Você tem algum tipo de Plano Dentário?',
    2 => '¿Tiene un plan dental de algún tipo?',
  ),
  85 => 
  array (
    0 => 'Yes, I do have a dental plan',
    1 => 'Sim, eu tenho um plano Dentário',
    2 => 'Sí, tengo un plan dental.',
  ),
  86 => 
  array (
    0 => 'No, I do not have a dental plan and I plan on paying in cash up front',
    1 => 'Não, eu não tenho um plano Dentário e planejo pagar em dinheiro',
    2 => 'No, no tengo un plan dental y planeo pagar en efectivo',
  ),
  87 => 
  array (
    0 => 'Please give us the information about your primary dental plan',
    1 => 'Por favor, informe-nos sobre o seu plano de seguro Dentário principal',
    2 => 'Por favor dénos la información sobre su plan dental primario',
  ),
  88 => 
  array (
    0 => 'If you are unsure about any information, please leave it blank',
    1 => 'Se você não tiver certeza sobre alguma informação, deixe-a em branco',
    2 => 'Si no está seguro de alguna información, déjela en blanco',
  ),
  89 => 
  array (
    0 => 'Dental Plan Company Name',
    1 => 'Nome da empresa do plano dentário',
    2 => '',
  ),
  90 => 
  array (
    0 => 'Dental Plan Company Address',
    1 => 'Endereço da empresa de plano dentário',
    2 => 'Nombre de la empresa del plano dental',
  ),
  91 => 
  array (
    0 => 'Dental Plan Company City',
    1 => 'Cidade da empresa de planos dentário',
    2 => 'Ciudad de la compañía del plan dental',
  ),
  92 => 
  array (
    0 => 'Dental Plan Company State',
    1 => 'Estado da empresa de plano dentário',
    2 => 'Estado de la compañía del plan dental',
  ),
  93 => 
  array (
    0 => 'Dental Plan Company Zip',
    1 => 'CEP da empresa de planos dentário',
    2 => 'Código postal de la compañía del plan dental'
  ),
  94 => 
  array (
    0 => 'Policyholder/Subscriber ID',
    1 => 'ID do titular da apólice/ identificação de membro',
    2 => 'ID del titular de la póliza/suscriptor',
  ),
  95 => 
  array (
    0 => 'Plan/Group Number',
    1 => 'Número do plano/grupo',
    2 => 'Número de plan/grupo',
  ),
  96 => 
  array (
    0 => 'Employer Name',
    1 => 'Nome do empregador',
    2 => 'Nombre del empleador',
  ),
  97 => 
  array (
    0 => 'Are you the policyholder of this plan?',
    1 => 'Você é o titular da apólice deste plano?',
    2 => '¿Eres el asegurado de este plan?',
  ),
  98 => 
  array (
    0 => 'Yes',
    1 => 'Sim',
    2 => 'Sí',
  ),
  99 => 
  array (
    0 => 'No',
    1 => 'Não',
    2 => 'No',
  ),
  100 => 
  array (
    0 => 'Please give the information about the policyholder of this plan',
    1 => 'Forneça as informações sobre o segurado deste plano',
    2 => 'Ponte la información sobre el titular de la póliza de este plan',
  ),
  101 => 
  array (
    0 => 'Policyholder First Name',
    1 => 'Primeeiro Nome do titular da apólice',
    2 => 'Primer Nombre del titular ',
  ),
  102 => 
  array (
    0 => 'Policyholder Middle Name',
    1 => 'Nome do meio do titular da apólice',
    2 => 'Segundo Nombre del titular de la poliza',
  ),
  103 => 
  array (
    0 => 'Policyholder Last Name',
    1 => 'Sobrenome do titular da apólice',
    2 => 'Apelido del titular de la poliza',
  ),
  104 => 
  array (
    0 => 'Policyholder Address',
    1 => 'Endereço do titular da apólice',
    2 => 'Dirección del titular de la póliza',
  ),
  105 => 
  array (
    0 => 'Policyholder City',
    1 => 'Cidade do seguro',
    2 => 'Ciudad del titular de la póliza',
  ),
  106 => 
  array (
    0 => 'Policyholder State',
    1 => 'Estado do segurado',
    2 => 'Estado del titular de la póliza',
  ),
  107 => 
  array (
    0 => 'Policyholder Zip',
    1 => 'CEP do segurado',
    2 => 'Código postal del titular de la póliza',
  ),
  108 => 
  array (
    0 => 'Policyholder Date of Birth',
    1 => 'Data de nascimento do segurado',
    2 => 'Fecha de nacimiento del titular de la póliza ',
  ),
  109 => 
  array (
    0 => 'Policyholder Gender',
    1 => 'Gênero do segurado',
    2 => 'Género del titular',
  ),
  110 => 
  array (
    0 => 'Relationship to Policyholder',
    1 => 'Relacionamento com o segurado',
    2 => 'Relación con el titular de la póliza',
  ),
  111 => 
  array (
    0 => 'Do you have a secondary dental plan or insurance?',
    1 => 'Você tem um plano dentário secundário ou seguro?',
    2 => '¿Tiene un plan o seguro dental secundario?',
  ),
  112 => 
  array (
    0 => 'Please give us the information about your secondary plan',
    1 => 'Forneça-nos as informações sobre o seu plano secundário',
    2 => 'Por favor, danos la información sobre tu plan secundario.',
  ),
  113 => 
  array (
    0 => 'Provides Medical Coverage?',
    1 => 'Oferece cobertura médica?',
    2 => '¿Proporciona cobertura médica?',
  ),
  114 => 
  array (
    0 => 'Provides Dental Coverage?',
    1 => 'Oferece cobertura dentário?',
    2 => 'Proporciona Cobertura Dental?',
  ),
  115 => 
  array (
    0 => 'Are you the policyholder of this secondary plan?',
    1 => 'Você é o titular deste plano secundário?',
    2 => '¿Eres el asegurado de este plan secundario?',
  ),
  116 => 
  array (
    0 => 'To help prevent any medical emergencies, we need to ask you a few questions about your medical health',
    1 => 'Para ajudar a prevenir emergências médicas, precisamos fazer algumas perguntas sobre sua saúde fisica',
    2 => 'Para ayudar a prevenir cualquier emergencia médica, debemos hacerle algunas preguntas sobre su salud médica.',
  ),
  117 => 
  array (
    0 => 'Are you currently breastfeeding?',
    1 => 'Você está amamentando no momento?',
    2 => '¿Está amamantando actualmente?',
  ),
  118 => 
  array (
    0 => 'Are you currently or attempting to be pregnant?',
    1 => 'Você está grávida ou tentando engravidar?',
    2 => '¿Está usted actualmente o tratando de estar embarazada?',
  ),
  119 => 
  array (
    0 => 'Do you normally take antibiotics before the start of a dental procedure?',
    1 => 'Você normalmente tomar antibióticos antes do início de um procedimento Dentário?',
    2 => '¿Toma normalmente antibióticos antes del inicio de un procedimiento dental?',
  ),
  120 => 
  array (
    0 => 'Do you have any history of Cancer?',
    1 => 'Você tem histórico de Câncer?',
    2 => '¿Tienes antecedentes de Cáncer?',
  ),
  121 => 
  array (
    0 => 'Do you have any',
    1 => 'Você tem algum',
    2 => 'Usted tiene alguna',
  ),
  122 => 
  array (
    0 => 'For example',
    1 => 'Por exemplo',
    2 => 'Por ejemplo',
  ),
  123 => 
  array (
    0 => 'Heart/Blood disorders',
    1 => 'Distúrbio cardíaco/sangue',
    2 => 'Trastornos cardíacos/sanguíneos',
  ),
  124 => 
  array (
    0 => 'Artificial Heart Valve(s)',
    1 => 'Válvulas Cardíacas Artificiais',
    2 => 'Válvula(s) de corazón artificial',
  ),
  125 => 
  array (
    0 => 'Congenital Heart Defects',
    1 => 'Defeitos Cardíacos Congênitos',
    2 => 'Defectos cardíacos congénitos',
  ),
  126 => 
  array (
    0 => 'Heart Murmurs',
    1 => 'Murmúrios Cardíacos',
    2 => 'Soplos cardíacos',
  ),
  127 => 
  array (
    0 => 'Angina',
    1 => 'Angina',
    2 => 'Angina',
  ),
  128 => 
  array (
    0 => 'Congestive Heart Failure',
    1 => 'Insuficiência Cardíaca Congestiva',
    2 => 'Insuficiencia cardíaca congestiva',
  ),
  129 => 
  array (
    0 => 'Heart Surgery',
    1 => 'Cirurgia Cardíaca',
    2 => 'Cirugía del corazón',
  ),
  130 => 
  array (
    0 => 'Pacemaker / defibrillator',
    1 => 'Marcapasso / desfibrilador',
    2 => 'Marcapasos / desfibrilador',
  ),
  131 => 
  array (
    0 => 'Bacterial Endocarditis',
    1 => 'Endocardite Bacteriana',
    2 => 'Endocarditis bacteriana',
  ),
  132 => 
  array (
    0 => 'Coronary Artery Disease',
    1 => 'Doença Arterial Coronariana',
    2 => 'Enfermedad de la arteria coronaria',
  ),
  133 => 
  array (
    0 => 'High Blood Pressure',
    1 => 'Pressão Arterial Alta',
    2 => 'Alta presión sanguínea',
  ),
  134 => 
  array (
    0 => 'Low Blood Pressure',
    1 => 'Pressão Arterial Baixa',
    2 => 'Presión arterial baja',
  ),
  135 => 
  array (
    0 => 'Abnormal Bleeding',
    1 => 'Sangramento Anormal',
    2 => 'Sangrado anormal',
  ),
  136 => 
  array (
    0 => 'Methemoglobinemia',
    1 => 'Metemoglobinemia',
    2 => 'Metahemoglobinemia',
  ),
  137 => 
  array (
    0 => 'Hemophillia',
    1 => 'Hemofilia',
    2 => 'Hemofilia',
  ),
  138 => 
  array (
    0 => 'Anemia',
    1 => 'Anemia',
    2 => 'Anemia',
  ),
  139 => 
  array (
    0 => 'Other Heart Conditions',
    1 => 'Outros Condições Cardíacas',
    2 => 'Otras condiciones del corazón',
  ),
  140 => 
  array (
    0 => 'Metabolic / Endocrine disorders',
    1 => 'distúrbio metabólico/endócrino',
    2 => 'Trastornos metabólicos/endocrinos',
  ),
  141 => 
  array (
    0 => 'Diabetes',
    1 => 'Diabetes',
    2 => 'Diabetes',
  ),
  142 => 
  array (
    0 => 'Hypothyroidism',
    1 => 'Hipotireoidismo',
    2 => 'Hipotiroidismo',
  ),
  143 => 
  array (
    0 => 'Hyperthyroidism',
    1 => 'Hipertireoidismo',
    2 => 'Hipertiroidismo',
  ),
  144 => 
  array (
    0 => 'Other metabolic or endocrine disorder',
    1 => 'Outros distúrbios metabólicos ou endócrinos',
    2 => 'Otro trastorno metabólico o endocrino',
  ),
  145 => 
  array (
    0 => 'Gastrointestinal disorders',
    1 => 'distúrbio gastrointestinais',
    2 => 'Desórdenes gastrointestinales',
  ),
  146 => 
  array (
    0 => 'G.E. Re-flux / Heartburn',
    1 => 'G.E. Refluxo / Azia',
    2 => 'G. E. reflujo / acidez estomacal',
  ),
  147 => 
  array (
    0 => 'Gastric Ulcers / Gastritis',
    1 => 'Úlceras Gástricas / Gastrite',
    2 => 'Úlceras Gástricas / Gastritis',
  ),
  148 => 
  array (
    0 => 'Inflammatory Bowell Disease',
    1 => 'Doença de Bowell Inflamatória',
    2 => 'Enfermedad inflamatoria intestinal',
  ),
  149 => 
  array (
    0 => 'Other Gastrointestinal disorders',
    1 => 'Outros Distúrbios Gastrointestinais',
    2 => 'Otros trastornos gastrointestinales',
  ),
  150 => 
  array (
    0 => 'Bone / Joint disorders',
    1 => 'distúrbio ósseo/articular',
    2 => 'Trastornos óseos/articulares',
  ),
  151 => 
  array (
    0 => 'Artificial joint',
    1 => 'Articulação artificial',
    2 => 'Articulación artificial',
  ),
  152 => 
  array (
    0 => 'Osteoporosis',
    1 => 'Osteoporose',
    2 => 'Osteoporosis',
  ),
  153 => 
  array (
    0 => 'History of Bisphosphonates (including Fosamax, Actonel, Boniva)',
    1 => 'Histórico de bifosfonatos (incluindo Fosamax, Actonel, Boniva)',
    2 => 'Historia de bisfosfonatos (incluidos Fosamax, Actonel, Boniva)',
  ),
  154 => 
  array (
    0 => 'Other Bone / Joint disorders',
    1 => 'Outros distúrbios ósseos/articulares',
    2 => 'Otros trastornos óseos/articulares',
  ),
  155 => 
  array (
    0 => 'Respiratory / Lung disorders',
    1 => 'Distúrbio respiratório/pulmão',
    2 => 'Trastornos respiratorios/pulmonares',
  ),
  156 => 
  array (
    0 => 'Asthma',
    1 => 'Asma',
    2 => 'Asma',
  ),
  157 => 
  array (
    0 => 'Emphysema or COPD',
    1 => 'Enfisema ou DPOC',
    2 => 'Enfisema o EPOC',
  ),
  158 => 
  array (
    0 => 'Bronchitis',
    1 => 'Bronquite',
    2 => 'Bronquitis',
  ),
  159 => 
  array (
    0 => 'Other Respiratory / Lung condition',
    1 => 'Outra condição respiratória / pulmonar',
    2 => 'Otra condición respiratoria/pulmonar',
  ),
  160 => 
  array (
    0 => 'Organ disorders',
    1 => 'Distúrbio de órgãos',
    2 => 'Trastornos de órganos',
  ),
  161 => 
  array (
    0 => 'Kidney Disease',
    1 => 'Doença Renal',
    2 => 'Enfermedad del riñon',
  ),
  162 => 
  array (
    0 => 'Liver disease',
    1 => 'Doença hepática',
    2 => 'Enfermedad del higado',
  ),
  163 => 
  array (
    0 => 'Eye disease (including glaucoma)',
    1 => 'Doença ocular (incluindo glaucoma)',
    2 => 'Enfermedad ocular (incluyendo glaucoma)',
  ),
  164 => 
  array (
    0 => 'Other organ disorder',
    1 => 'Outros distúrbios de órgãos ',
    2 => 'Otro trastorno de órganos',
  ),
  165 => 
  array (
    0 => 'Infectious disease',
    1 => 'doença infecciosa',
    2 => '',
  ),
  166 => 
  array (
    0 => 'AIDS / HIV',
    1 => 'AIDS / HIV',
    2 => 'AIDS / HIV',
  ),
  167 => 
  array (
    0 => 'Hepatitis',
    1 => 'Hepatite',
    2 => 'Hepatitis',
  ),
  168 => 
  array (
    0 => 'Other Sexually Transmitted Disease',
    1 => 'Outra Doença Sexualmente Transmissível',
    2 => 'Otras enfermedades de transmisión sexual',
  ),
  169 => 
  array (
    0 => 'Tuberculosis',
    1 => 'Tuberculose',
    2 => 'Tuberculosis',
  ),
  170 => 
  array (
    0 => 'Other infectious disease ',
    1 => 'Outra doença infecciosa',
    2 => 'Otra enfermedad infecciosa',
  ),
  171 => 
  array (
    0 => 'Neurological disorders',
    1 => 'Distúrbio Neurológico',
    2 => 'Desórdenes neurológicos',
  ),
  172 => 
  array (
    0 => 'Epilepsy',
    1 => 'Epilepsia',
    2 => 'Epilepsia',
  ),
  173 => 
  array (
    0 => 'Tourette Syndrome',
    1 => 'Síndrome de Tourette',
    2 => 'Síndrome de Tourette',
  ),
  174 => 
  array (
    0 => 'Stroke',
    1 => 'Acidente Vascular Cerebral',
    2 => 'Accidente vascular cerebral',
  ),
  175 => 
  array (
    0 => 'Migraine',
    1 => 'Migrina',
    2 => 'Migraña',
  ),
  176 => 
  array (
    0 => 'Other Neurological disorders',
    1 => 'Outros distúrbios neurológicos',
    2 => 'Otros trastornos neurológicos',
  ),
  177 => 
  array (
    0 => 'Immune diseases',
    1 => 'Doença imunológica',
    2 => 'Enfermedades inmunológicas',
  ),
  178 => 
  array (
    0 => 'Lupus',
    1 => 'Lúpus',
    2 => 'Lupus',
  ),
  179 => 
  array (
    0 => 'Rheumatoid Arthritis',
    1 => 'Artrite Reumatoide',
    2 => 'Artritis Reumatoide',
  ),
  180 => 
  array (
    0 => 'Sjögren’s syndrome',
    1 => 'Síndrome de Sjögren',
    2 => 'Síndrome de Sjögren',
  ),
  181 => 
  array (
    0 => 'Myasthenia Gravis',
    1 => 'Miastenia Gravis',
    2 => 'Miastenia Gravis',
  ),
  182 => 
  array (
    0 => 'Other immune diseases',
    1 => 'Outras doenças imunológicas',
    2 => 'Otras enfermedades inmunologicas',
  ),
  183 => 
  array (
    0 => 'Psychotic / Behavioral disorders',
    1 => 'Distúrbio psicótico/comportamental',
    2 => '',
  ),
  184 => 
  array (
    0 => 'ADD / ADHD',
    1 => 'ADD / TDAH',
    2 => 'ADD / TDAH',
  ),
  185 => 
  array (
    0 => 'Depression',
    1 => 'Depressão',
    2 => 'Depresión',
  ),
  186 => 
  array (
    0 => 'Anxiety / Panic Attacks',
    1 => 'Ansiedade / Ataques de Pânico',
    2 => 'Ansiedad / Ataques de pánico',
  ),
  187 => 
  array (
    0 => 'Eating disorder',
    1 => 'Transtorno alimentar',
    2 => 'Desorden alimenticio',
  ),
  188 => 
  array (
    0 => 'Other behavioral disorders',
    1 => 'Outros distúrbios comportamentais',
    2 => 'Otros trastornos del comportamiento',
  ),
  189 => 
  array (
    0 => 'Yes, I do have one of those conditions',
    1 => 'Sim, eu tenho uma dessas condições',
    2 => 'Sí, tengo una de esas condiciones.',
  ),
  190 => 
  array (
    0 => 'No, I don\'t have any one of those conditions',
    1 => 'Não, eu não tenho uma dessas condições',
    2 => 'No, no tengo ninguna de estas condiciones.',
  ),
  191 => 
  array (
    0 => 'Do you have any Metabolic / Endocrine disorders?',
    1 => 'Você tem algum distúrbio metabólico/endócrino?',
    2 => '¿Tiene algún trastorno metabólico/endocrino?',
  ),
  192 => 
  array (
    0 => 'Please list your medications. If you are taking none, please leave this form blank.',
    1 => 'Por favor, liste seus medicamentos. Se você não estiver tomando nenhum, deixe este formulário em branco.',
    2 => 'Por favor enumere sus medicamentos. Si no está tomando ninguno, deje este formulario en blanco.',
  ),
  193 => 
  array (
    0 => 'Name of Medication',
    1 => 'Nome do Medicamento',
    2 => 'Nombre del medicamento',
  ),
  194 => 
  array (
    0 => 'Why are you taking it?',
    1 => 'Por que você está tomando?',
    2 => 'Porque estas tomando',
  ),
  195 => 
  array (
    0 => 'Please list your allergies. If you do not have any known allergies, please leave this form blank.',
    1 => 'Por favor, liste suas alergias. Se você não tem alergias conhecidas, deixe este formulário em branco.',
    2 => 'Por favor enumere sus alergias. Si no tiene ninguna alergia conocida, deje este formulario en blanco',
  ),
  196 => 
  array (
    0 => 'What you are allergic to',
    1 => 'Ao que você é alérgico',
    2 => 'A qué eres alérgico',
  ),
  197 => 
  array (
    0 => 'How severe is it?',
    1 => 'Quão grave é isso?',
    2 => '¿Qué tan grave es?',
  ),
  198 => 
  array (
    0 => 'Mild (Rash, itching, stomach ache)',
    1 => 'Leve (erupção na pele, coceira, dor de estômago)',
    2 => 'Leve (Erupción, picazón, dolor de estómago)',
  ),
  199 => 
  array (
    0 => 'Moderate (scratchy throat, watery / itchy eyes)',
    1 => 'Moderado (garganta arranhada, olhos lacrimejantes / coceira)',
    2 => 'Moderado (garganta áspera, ojos llorosos / picazón)',
  ),
  200 => 
  array (
    0 => 'Severe (Anaphylaxis, difficulty swallowing or breathing)',
    1 => 'Grave (anafilaxia, dificuldade em engolir ou respirar)',
    2 => 'Grave (anafilaxia, dificultad para tragar o respirar)',
  ),
  201 => 
  array (
    0 => 'I don\'t know, somebody told me I was allergic to it',
    1 => 'Eu não sei, alguém me disse que eu era alérgico a isso',
    2 => 'No sé, alguien me dijo que era alérgico a eso.',
  ),
  202 => 
  array (
    0 => 'Please sign the consent form HIPAA CONSENT FORM I consent to the use or disclosure of my protected health information (PHI) for the purpose of treatment, payment, and health care operations.',
    1 => 'Assine o formulário de consentimento  FORMULÁRIO DE CONSENTIMENTO HIPAA  Eu concordo com o uso ou divulgação de minhas informações de saúde protegidas (PHI) para fins de tratamento, pagamento e operações de saúde.',
    2 => 'Firme el formulario de consentimiento FORMULARIO DE CONSENTIMIENTO DE HIPAA Doy mi consentimiento para el uso o divulgación de mi información médica protegida (PHI) con el propósito de tratamiento, pago y operaciones de atención médica',
  ),
  203 => 
  array (
    0 => 'I UNDERSTAND',
    1 => 'EU ENTENDO',
    2 => 'YO ENTIENDO',
  ),
  204 => 
  array (
    0 => 'Service to me may be conditioned upon my consent as evidenced by my signature on this document.',
    1 => 'O meu atendimento pode estar condicionada ao meu consentimento, conforme evidenciado pela minha assinatura neste documento.',
    2 => 'El servicio para mí puede estar condicionado a mi consentimiento como lo demuestra mi firma en este documento',
  ),
  205 => 
  array (
    0 => 'I have the right to request a restriction as to how my PHI is used or disclosed to carry out treatment, payment, or health care operations of the practice. This practice is not required to agree to the restrictions that I may request. However, if this practice agrees to a restriction that I request, the restriction is binding on this practice.',
    1 => 'Tenho o direito de solicitar uma restrição sobre como meu PHI é usado ou divulgado para realizar operações de tratamento, pagamento ou cuidados de do consultório. Este consultório não é obrigada a concordar com as restrições que posso solicitar. No entanto, se esse consultório concordar com uma restrição que eu solicito, a restrição é vinculativa para esse consultório. ',
    2 => 'Tengo derecho a solicitar una restricción en cuanto a cómo se usa o divulga mi PHI para llevar a cabo el tratamiento, el pago o las operaciones de atención médica de la práctica. Esta práctica no está obligada a aceptar las restricciones que pueda solicitar. Sin embargo, si esta práctica está de acuerdo con una restricción que solicito, la restricción es vinculante para esta práctica.',
  ),
  206 => 
  array (
    0 => 'I have the right to revoke this consent, in writing, at any time, except to the extent that this practice has taken action in reliance on this consent. ',
    1 => 'Tenho o direito de revogar esse consentimento, por escrito, a qualquer momento, exceto na medida em que esse consultório tenha tomado medidas com base nesse consentimento. ',
    2 => 'Tengo derecho a revocar este consentimiento, por escrito, en cualquier momento, excepto en la medida en que esta práctica haya actuado basándose en este consentimiento.',
  ),
  207 => 
  array (
    0 => 'My PHI means health information, including my demographic information, collected from me and created or received by my doctor, another health care provider, a health plan, or a health care clearinghouse. This PHI relates to my past, present or future physical or mental health or condition and identifies me; or, there is a reasonable basis to believe the information may identify me.',
    1 => 'Meu PHI significa informações de saúde, incluindo minhas informações demográficas, coletadas de mim e criadas ou recebidas pelo meu médico, outro prestador de saúde, um plano de saúde outra instituicao de compensação de cuidados de saúde. Este PHI está relacionado à minha saúde ou condição física ou mental passada, presente ou futura e me identifica; ou, há uma base razoável para acreditar que as informações podem me identificar.',
    2 => 'Mi PHI significa información de salud, incluida mi información demográfica, recopilada de mí y creada o recibida por mi médico, otro proveedor de atención médica, un plan de salud o un centro de información de atención médica. Esta PHI se relaciona con mi salud o condición física o mental pasada, presente o futura y me identifica; o existe una base razonable para creer que la información puede identificarme.',
  ),
  208 => 
  array (
    0 => 'THE NOTICE OF PRIVACY PRACTICES DESCRIBES',
    1 => 'O AVISO DE PRÁTICAS DE PRIVACIDADE DESCREVE',
    2 => 'EL AVISO DE PRÁCTICAS DE PRIVACIDAD DESCRIBE',
  ),
  209 => 
  array (
    0 => 'The types of uses and disclosures of my PHI that will occur in my treatment, payment of my bills or in the performance of health care operations performed by '.$PRACTICE_NAME.'.',
    1 => 'Os tipos de usos e divulgações do meu PHI que ocorrerão no meu tratamento, pagamento de minhas contas ou no desempenho das operações de saúde realizadas pela '.$PRACTICE_NAME.'.',
    2 => 'Los tipos de usos y divulgaciones de mi PHI que ocurrirán en mi tratamiento, pago de mis facturas o en la realización de operaciones de atención médica realizadas por '.$PRACTICE_NAME.'.',
  ),
  210 => 
  array (
    0 => 'My rights and the duties of '.$PRACTICE_NAME.' with respect to my PHI.',
    1 => 'Meus direitos e os deveres da '.$PRACTICE_NAME.' em relação ao meu PHI.',
    2 => 'Mis derechos y deberes de '.$PRACTICE_NAME.' con respecto a mi PHI.',
  ),
  211 => 
  array (
    0 => 'This practice reserves the right to change its privacy practices. For any information on current or revised notices, please call our office.',
    1 => 'Esta prática se reserva o direito de alterar suas práticas de privacidade. Para qualquer informação sobre avisos atuais ou revisados, ligue para o nosso escritório.',
    2 => 'Esta práctica se reserva el derecho de cambiar sus prácticas de privacidad. Para cualquier información sobre avisos actuales o revisados, llame a nuestra oficina.',
  ),
  212 => 
  array (
    0 => 'Please sign below to agree with the privacy policy',
    1 => 'Assine abaixo para concordar com a política de privacidade ',
    2 => 'Por favor firme abajo para estar de acuerdo con la política de privacidad',
  ),
  213 => 
  array (
    0 => 'Thank you! Somebody from our office should contact you within 24 hours. If not, please call '.$PRACTICE_PHONE_NUMBER.'.',
    1 => 'Obrigado! Alguém do nosso escritório deve entrar em contato com você dentro de 24 horas. Caso contrário, ligue para '.$PRACTICE_PHONE_NUMBER.'.',
    2 => '¡Gracias! Alguien de nuestra oficina debe comunicarse con usted dentro de las 24 horas. De lo contrario, llame al '.$PRACTICE_PHONE_NUMBER.'.',
  ),
  214 => 
  array (
    0 => 'You may now close the tab or window.',
    1 => 'Agora você pode fechar essa seção ou janela.',
    2 => 'Ahora puede cerrar la ventana.',
  ),
  215 => 
  array (
    0 => 'Yay! 😄 We are off to a great start!',
    1 => 'Uhu! 😄 Nós começamos bem! ',
    2 => '¡Hurra! 😄 ¡Hemos tenido un gran comienzo!',
  ),
  216 => 
  array (
    0 => 'To help us give you the best experience for you first visit, we need to ask you a few questions.',
    1 => 'Para ajudá lo a ter a melhor experiência na sua primeira visita, precisamos lhe fazer algumas perguntas.',
    2 => 'Para ayudarnos a darte la mejor experiencia en su primera visita, debemos hacerle algunas preguntas.',
  ),
  217 => 
  array (
    0 => 'Do you have any issues or concerns about anything in your mouth?',
    1 => 'Você tem algum problema ou preocupação sobre alguma coisa na sua boca?',
    2 => 'Tienes algun problema o inquietud de algo en su boca?',
  ),
  218 => 
  array (
    0 => 'None at all',
    1 => 'Não',
    2 => 'No, ninguno',
  ),
  219 => 
  array (
    0 => 'I have a sensitive tooth',
    1 => 'Eu tenho um dente sensivel',
    2 => 'Tengo un diente sensible ',
  ),
  220 => 
  array (
    0 => 'I don\'t like how my teeth look',
    1 => 'Não gosto como meus dentes parecem',
    2 => 'No me gusta como se ven mis dientes',
  ),
  221 => 
  array (
    0 => 'I have a broken/chipped tooth I wish to have addressed',
    1 => 'Estou com dente quebrado / trincado',
    2 => 'Tengo un diente roto/astillado que deseo que me arreglen',
  ),
  222 => 
  array (
    0 => 'Where is your sensitive tooth?',
    1 => 'Onde está seu dente sensível?',
    2 => '¿Dónde está tu diente sensible?',
  ),
  223 => 
  array (
    0 => 'Where is your broken/chipped tooth?',
    1 => 'Onde está seu dente quebrado / lascado?',
    2 => '¿Dónde está tu diente roto/astillado?',
  ),
  224 => 
  array (
    0 => 'What are you looking to get done?',
    1 => 'O que você pretende fazer?',
    2 => 'Oque deseas hacer?',
  ),
  225 => 
  array (
    0 => 'Just a Cleaning and Exam',
    1 => 'A penas limpeza e exame',
    2 => 'Solamente una limpieza e exame',
  ),
  226 => 
  array (
    0 => 'I need a filling, crown or denture',
    1 => 'Preciso de obturação, coroa ou dentadura',
    2 => 'Necesito una obturación, corona o dentadura',
  ),
  227 => 
  array (
    0 => 'I want my teeth to look better',
    1 => 'Quero que meus dentes tenham uma aparência melhor',
    2 => 'Quiero que mis dientes se vean mejores ',
  ),
  228 => 
  array (
    0 => 'I don\'t know yet',
    1 => 'El não sei ainda',
    2 => 'No se todavia ',
  ),
  229 => 
  array (
    0 => 'Do you have any past issues with the dentist?',
    1 => 'Você tem algum problema com algum dentista no passado?',
    2 => 'Ya has tenido problema con algun dentista antes?',
  ),
  230 => 
  array (
    0 => 'No issues at all',
    1 => 'Não',
    2 => 'No, nunca  ',
  ),
  231 => 
  array (
    0 => 'No issues, but I don\'t like seeing the dentist though',
    1 => 'Não, mas eu não gosto de ir ao dentista',
    2 => 'Nunca he tenido problema antes, pero no me gusta ir al dentista',
  ),
  232 => 
  array (
    0 => 'I have had an issue in the dental chair',
    1 => 'Eu tive um problema na cadeira de dentista',
    2 => 'He tenido un problema en el dentista',
  ),
  233 => 
  array (
    0 => 'I am extremely scared of the dentist',
    1 => 'Eu tenho muito medo de dentista',
    2 => 'Tengo mucho miedo de ir al dentista ',
  ),
  234 => 
  array (
    0 => 'When was your last cleaning?',
    1 => 'Quando foi sua última limpeza?',
    2 => 'Cuando fue su ultima limpieza?',
  ),
  235 => 
  array (
    0 => 'About 6 months to 12 months ago',
    1 => 'Entre 6 a 12 meses atrás',
    2 => 'Cerca de 6 meses a 12 meses atras ',
  ),
  236 => 
  array (
    0 => 'Less than 6 months ago',
    1 => 'Menos de 6 meses atrás',
    2 => 'Menos de 6 meses atras',
  ),
  237 => 
  array (
    0 => 'More than a year ago',
    1 => 'Mais de 1 ano atrás',
    2 => 'Hace más de un año',
  ),
  238 => 
  array (
    0 => 'More than 2 years ago',
    1 => 'Mais de 2 anos atrás',
    2 => 'Hace más de dos años',
  ),
  239 => 
  array (
    0 => 'More than 10 years ago / I can\'t remember',
    1 => 'Mais de 10 anos atrás / não lembro',
    2 => 'Hace más de diez años/ no me recuerdo',
  ),
  240 => 
  array (
    0 => 'Have you ever had "scaling and root planing" or a "deep cleaning" done before?',
    1 => 'Você já fez "planejamento de raiz" ou uma "limpeza profunda" antes?',
    2 => '¿Alguna vez has hecho una "limpieza profunda"?',
  ),
  241 => 
  array (
    0 => 'Yes, I had it done less than 2 years ago',
    1 => 'Sim, há menos de 2 anos',
    2 => 'Si, tuvo una menos de 2 años atras',
  ),
  242 => 
  array (
    0 => 'Yes, but I had it done more than 2 years ago',
    1 => 'Sim, há mais de 2 anos',
    2 => 'Si, pero tuve una mas de 2 años atras',
  ),
  243 => 
  array (
    0 => 'No, I have never had it done before',
    1 => 'Não, nunca fiz',
    2 => 'No, nunca he tenido una antes ',
  ),
  244 => 
  array (
    0 => 'I don\'t even know what you are talking about',
    1 => 'Não, sei o que isso significa',
    2 => 'No se lo que estas me preguntando ',
  ),
  245 => 
  array (
    0 => 'How often do you brush?',
    1 => 'Com que frequência você escova os dentes?',
    2 => '¿Con qué frecuencia te cepillas?',
  ),
  246 => 
  array (
    0 => 'Twice a day',
    1 => 'Duas vezes ao dia',
    2 => 'Dos veces al dia ',
  ),
  247 => 
  array (
    0 => 'Daily',
    1 => 'Diariamente',
    2 => 'Diariamente ',
  ),
  248 => 
  array (
    0 => 'Weekly',
    1 => 'Semanalmente',
    2 => 'Semanalmente ',
  ),
  249 => 
  array (
    0 => 'Never',
    1 => 'Nunca',
    2 => 'Nunca',
  ),
  250 => 
  array (
    0 => 'How often do you floss?',
    1 => 'Com que frequência você usa fio dental?',
    2 => '¿Con qué frecuencia usa el hilo dental?',
  ),
  251 => 
  array (
    0 => 'Does your gum bleed when you brush or floss?',
    1 => 'Sua gengiva sangra quando você escova ou use o fio dental?',
    2 => '¿Le sangra la encía cuando se cepilla los dientes o usa hilo dental?',
  ),
  252 => 
  array (
    0 => 'Never',
    1 => 'Nunca',
    2 => 'Nunca',
  ),
  253 => 
  array (
    0 => 'Sometimes',
    1 => 'Às vezes',
    2 => 'Algunas veces',
  ),
  254 => 
  array (
    0 => 'Rarely',
    1 => 'Raramente',
    2 => 'Raramente',
  ),
  255 => 
  array (
    0 => 'Every time',
    1 => 'O tempo todo',
    2 => 'Todas las veces',
  ),
  256 => 
  array (
    0 => 'Do you have headaches?',
    1 => 'Você tem dores de cabeça?',
    2 => '¿Tienes dolores de cabeza?',
  ),
  257 => 
  array (
    0 => 'Never',
    1 => 'Nunca',
    2 => 'Nunca',
  ),
  258 => 
  array (
    0 => 'Sometimes',
    1 => 'Às vezes',
    2 => 'Algunas veces',
  ),
  259 => 
  array (
    0 => 'All the time',
    1 => 'O tempo todo',
    2 => 'Todo el tiempo',
  ),
  260 => 
  array (
    0 => 'Do you smoke?',
    1 => 'Voce fuma?',
    2 => '¿Fumas?',
  ),
  261 => 
  array (
    0 => 'I never smoked',
    1 => 'Nunca fumei',
    2 => 'Nunca fumé',
  ),
  262 => 
  array (
    0 => 'I used to smoke, but I already quit',
    1 => 'Eu costumava fumei, mas parei',
    2 => 'Estava acostumbrado a fumar, pero ahora no mas',
  ),
  263 => 
  array (
    0 => 'I do smoke, but rarely',
    1 => 'Eu fumo raramente',
    2 => 'Fumo, pero de vez en cuando',
  ),
  264 => 
  array (
    0 => 'I smoke at least once a day',
    1 => 'Eu fumo pelo menos uma vez ao dia',
    2 => 'Fumo al menos una vez al dia ',
  ),
  265 => 
  array (
    0 => 'Do you know if you grind at night?',
    1 => 'Você sabe se seus dentes rangem à noite?',
    2 => '¿Sabes si mueles por la noche?',
  ),
  266 => 
  array (
    0 => 'Never',
    1 => 'Nunca',
    2 => 'Nunca',
  ),
  267 => 
  array (
    0 => 'No clue',
    1 => 'Não tenho idéia',
    2 => 'No se ',
  ),
  268 => 
  array (
    0 => 'Sometimes',
    1 => 'Às vezes',
    2 => 'Algunas veces',
  ),
  269 => 
  array (
    0 => 'I used to grind my teeth, but not anymore',
    1 => 'Eu costumava ranger os dentes, mas não mais',
    2 => 'Solia a rechinar los dientes, pero ya no',
  ),
  270 => 
  array (
    0 => 'Every night',
    1 => 'Toda a noite',
    2 => 'Todas las noches ',
  ),
  271 => 
  array (
    0 => 'Have you ever had braces before?',
    1 => 'Você já usou aparelhos?',
    2 => '¿Alguna vez has tenido frenos antes?',
  ),
  272 => 
  array (
    0 => 'No, never',
    1 => 'Não, nunca',
    2 => 'No, nunca',
  ),
  273 => 
  array (
    0 => 'Yes, back when I was a kid many years ago',
    1 => 'Sim, quando criança',
    2 => 'Si, años atrás cuando era nino(a)',
  ),
  274 => 
  array (
    0 => 'Yes, I had it done just a few years ago',
    1 => 'Sim, foi colocado ha alguns anos',
    2 => 'Si, lo has tenido algunos años atrás ',
  ),
  275 => 
  array (
    0 => 'Do you have any trouble sleeping at night?',
    1 => 'Você tem algum problema para dormir à noite?',
    2 => '¿Tienes algún problema para dormir por la noche?',
  ),
  276 => 
  array (
    0 => 'No, I have no issues with sleeping',
    1 => 'Não, nenhum problema para dormir',
    2 => 'No tengo ningun problema en dormir en la noche. ',
  ),
  277 => 
  array (
    0 => 'Yes, I sometimes wake up in the middle of the night',
    1 => 'Sim às vezes eu acordo no meio de noite',
    2 => 'Si, as vezes me despierto durante la noche. ',
  ),
  278 => 
  array (
    0 => 'I have been diagnosed with Sleep apnea',
    1 => 'Sim eu fui diagnosticado com apnéia do sono',
    2 => 'Me han diagnosticado con apnea del sueño.',
  ),
  279 => 
  array (
    0 => 'I have a different sleep issue',
    1 => 'Eu tenho um problema diferente para dormir',
    2 => 'Tengo un problema de sueño diferente',
  ),
  280 => 
  array (
    0 => 'To help us schedule you for the best times, we need to know some of your preferences',
    1 => 'Para nos ajudar a programá-lo para os melhores horários, precisamos saber algumas de suas preferências',
    2 => 'Para ayudarnos a programarlo en los mejores horarios, necesitamos saber algunas de sus preferencias',
  ),
  281 => 
  array (
    0 => 'Home Telephone Number',
    1 => 'Número de telefone residencial',
    2 => 'Numero de telefono de casa',
  ),
  282 => 
  array (
    0 => 'Cell Telephone Number',
    1 => 'Número de telefone celular',
    2 => 'Número de teléfono celular',
  ),
  283 => 
  array (
    0 => 'Work Number',
    1 => 'Número de Trabalho',
    2 => 'Número de Trabajo',
  ),
  284 => 
  array (
    0 => 'Personal Email Address',
    1 => 'Endereço de Email Pessoal',
    2 => 'Correo electrónico personal',
  ),
  285 => 
  array (
    0 => 'Work Email Address',
    1 => 'Endereço de e-mail comercial',
    2 => 'Correo electrónico del trabajo',
  ),
  286 => 
  array (
    0 => 'Facebook Address (user ID or URL)',
    1 => 'Endereço do Facebook (ID de usuário ou URL)',
    2 => 'Dirección de Facebook (ID de usuario o URL)',
  ),
  287 => 
  array (
    0 => 'Instagram ID',
    1 => 'Instagram ID',
    2 => 'Instagram ID',
  ),
  288 => 
  array (
    0 => 'WhatsApp ID',
    1 => 'WhatsApp ID',
    2 => 'WhatsApp ID',
  ),
  289 => 
  array (
    0 => 'How would you like to be contacted?',
    1 => 'Como você gostaria de ser contactado?',
    2 => '¿Cómo le gustaría ser contactado?',
  ),
  290 => 
  array (
    0 => 'Call me at my home number',
    1 => 'Me ligue no número da minha casa',
    2 => 'Llamarme por telefono de casa',
  ),
  291 => 
  array (
    0 => 'Call me at my cell phone number',
    1 => 'Me ligue no meu celular',
    2 => 'Llamarme por celular',
  ),
  292 => 
  array (
    0 => 'Call me at work',
    1 => 'Me liga no trabalho',
    2 => 'Llamarme por telefono del trabajo',
  ),
  293 => 
  array (
    0 => 'Text me at my cell phone number',
    1 => 'Me mande uma mensagem no meu número de celular',
    2 => 'Mandarme un mensaje por celular',
  ),
  294 => 
  array (
    0 => 'Email me at my personal address',
    1 => 'Envie-me um e-mail para o meu endereço pessoal',
    2 => 'Mandarme un mensaje por email personal',
  ),
  295 => 
  array (
    0 => 'Email me at my work address',
    1 => 'Envie-me um e-mail para o meu endereço de trabalho',
    2 => 'Mandarme un mensaje por email del trabajo',
  ),
  296 => 
  array (
    0 => 'Send me a Facebook message',
    1 => 'Envie-me uma mensagem no Facebook',
    2 => 'Mandame un mensaje por Facebook',
  ),
  297 => 
  array (
    0 => 'Send me a Instagram message',
    1 => 'Me mande uma mensagem no Instagram',
    2 => 'Mandame un mensaje por Instagram',
  ),
  298 => 
  array (
    0 => 'Send me a WhatsApp message',
    1 => 'Envie-me uma mensagem do WhatsApp',
    2 => 'Mandame un mensaje por WhatsApp',
  ),
  299 => 
  array (
    0 => 'Which days are you available?',
    1 => 'Em que dias você está disponível?',
    2 => '¿Qué días estás disponible?',
  ),
  300 => 
  array (
    0 => 'Sundays',
    1 => 'Domingos',
    2 => 'Domingos',
  ),
  301 => 
  array (
    0 => 'Mondays',
    1 => 'Segundas',
    2 => 'Lunes',
  ),
  302 => 
  array (
    0 => 'Tuesdays',
    1 => 'Terças feiras',
    2 => 'Martes',
  ),
  303 => 
  array (
    0 => 'Wednesdays',
    1 => 'Quartas feiras',
    2 => 'Miércoles',
  ),
  304 => 
  array (
    0 => 'Thursdays',
    1 => 'Quintas-feiras',
    2 => 'Jueves',
  ),
  305 => 
  array (
    0 => 'Fridays',
    1 => 'Sextas feiras',
    2 => 'Viernes',
  ),
  306 => 
  array (
    0 => 'Saturdays',
    1 => 'Sábados',
    2 => 'Sábados',
  ),
  307 => 
  array (
    0 => 'Which time of days are you available?',
    1 => 'Em que horas do dia você está disponível?',
    2 => '¿A qué hora del día está disponible?',
  ),
  308 => 
  array (
    0 => 'Early Mornings (5AM-7AM)',
    1 => 'Manhãs cedo (5h às 7h)',
    2 => 'Temprano en la mañana (5AM-7AM)',
  ),
  309 => 
  array (
    0 => 'Mornings (7AM-9AM)',
    1 => 'Manhãs (7h-9h)',
    2 => 'Mañanas (7AM-9AM)',
  ),
  310 => 
  array (
    0 => 'Early Afternoons (9AM-1PM)',
    1 => 'No início da tarde (9h-13h)',
    2 => 'Temprano en la tarde (9AM-1PM)',
  ),
  311 => 
  array (
    0 => 'Late Afternoons (1PM-4PM)',
    1 => 'Fim da tarde (13h às 16h)',
    2 => 'Tardes (1PM-4PM)',
  ),
  312 => 
  array (
    0 => 'Evenings (4PM-6PM)',
    1 => 'Noites (16h-18h)',
    2 => 'Temprano en la noche (4PM-6PM)',
  ),
  313 => 
  array (
    0 => 'Late Evenings (6PM-8PM)',
    1 => 'Tarde da noite (18h às 20h)',
    2 => 'Tarde en la noche (6PM-8PM)',
  ),
  314 =>
  array (
    0 => 'Is there anything else you would like to inform the doctor before the first appointment?',
    1 => 'Há mais alguma coisa que você gostaria de informar ao médico antes da primeira consulta?',
    2 => '¿Hay algo más que le gustaría informar al médico antes de la primera cita?',
  ),
)

?>
