<?php
require('Common.php');
printHeader("Your Medical Conditions");
?>
<form name="MedicalConditions" action="Medications.php" method="POST">
    <div class="container mt-3">
        <div class="row mb-4">
            <div class="col-12">
                <h2 id="condTitle"></h2>
            </div>
            <div class="col-12">
                <p id="condExample"></p>
            </div>
        </div>
  
        <div class="row justify-content-between" id="hasCondOrNotButRow">
            <div class="col-auto mb-4">
                <button type="button" class="btn btn-danger btn-lg" onclick="handleShowConditons()"><?php echo $translationArray[189][$LANG_ID]?></button>
            </div>
            <div class="col-auto">
                <button type="button" class="btn btn-success btn-lg" onclick="handleNextCondition()"><?php echo $translationArray[190][$LANG_ID]?></button>
            </div>
        </div>
        
        <div class="row mt-3" id="selectCondsHeader" style="display:none">
            <div class="col-12">
                <h2>Please select the conditions that you have:</h2>
            </div>
        </div>
        
        <div class="row" id="allCondRow" style="display:none">
            <div class="col-12">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">Default checkbox</label>
                </div>
            </div>
            
        </div>
        
        <div class="row mt-4 mb-2" id="doneButRow" style="display:none">
             <div class="col-12">
                <button type="button" class="btn btn-success btn-lg btn-block" onclick="handleFinishedWithCondition()">Done</button>
             </div>
        </div>
        
        <div class="row mt-4 mb-2" id="finishRow" style="display:none">
             <div class="col-12">
                <button type="submit" class="btn btn-success btn-lg btn-block"><?php echo $translationArray[30][$LANG_ID]?></button>
             </div>
        </div>
        
        <div id="hideInMe"></div>
        
  </div>
  
  <script>
        var conditionTrArray = [
    {
        English: "Do you have any",
        Portuguese: "Você tem algum",
        Spanish: "¿Tiene alguno"
    },
    {
        English: "For example",
        Portuguese: "Por exemplo",
        Spanish: "Por ejemplo"
    },
    {
        English: "Heart/Blood disorders",
        Portuguese: "Distúrbio cardíaco/sangue",
        Spanish: "Trastornos cardíacos/sanguíneos"
    },
    {
        English: "Artificial Heart Valve(s)",
        Portuguese: "Válvulas Cardíacas Artificiais",
        Spanish: "Válvulas cardíacas artificiales"
    },
    {
        English: "Congenital Heart Defects",
        Portuguese: "Defeitos Cardíacos Congênitos",
        Spanish: "Defectos cardíacos congénitos"
    },
    {
        English: "Heart Murmurs",
        Portuguese: "Murmúrios Cardíacos",
        Spanish: "Soplos cardíacos"
    },
    {
        English: "Angina",
        Portuguese: "Angina",
        Spanish: "Angina"
    },
    {
        English: "Congestive Heart Failure",
        Portuguese: "Insuficiência Cardíaca Congestiva",
        Spanish: "Insuficiencia cardíaca congestiva"
    },
    {
        English: "Heart Surgery",
        Portuguese: "Cirurgia Cardíaca",
        Spanish: "Cirugía cardíaca"
    },
    {
        English: "Pacemaker / defibrillator",
        Portuguese: "Marcapasso / desfibrilador",
        Spanish: "Marcapasos/desfibrilador"
    },
    {
        English: "Bacterial Endocarditis",
        Portuguese: "Endocardite Bacteriana",
        Spanish: "Endocarditis bacteriana"
    },
    {
        English: "Coronary Artery Disease",
        Portuguese: "Doença Arterial Coronariana",
        Spanish: "Enfermedad de las arterias coronarias"
    },
    {
        English: "High Blood Pressure",
        Portuguese: "Pressão Arterial Alta",
        Spanish: "Presión arterial alta"
    },
    {
        English: "Low Blood Pressure",
        Portuguese: "Pressão Arterial Baixa",
        Spanish: "Presión arterial baja"
    },
    {
        English: "Abnormal Bleeding",
        Portuguese: "Sangramento Anormal",
        Spanish: "Sangrado anormal"
    },
    {
        English: "Methemoglobinemia",
        Portuguese: "Metemoglobinemia",
        Spanish: "Metahemoglobinemia"
    },
    {
        English: "Hemophillia",
        Portuguese: "Hemofilia",
        Spanish: "Hemofilia"
    },
    {
        English: "Anemia",
        Portuguese: "Anemia",
        Spanish: "Anemia"
    },
    {
        English: "Other Heart Conditions",
        Portuguese: "Outros Condições Cardíacas",
        Spanish: "Otras condiciones cardíacas"
    },
    {
        English: "Metabolic / Endocrine disorders",
        Portuguese: "distúrbio metabólico/endócrino",
        Spanish: "Trastornos metabólicos/endocrinos"
    },
    {
        English: "Diabetes",
        Portuguese: "Diabetes",
        Spanish: "Diabetes"
    },
    {
        English: "Hypothyroidism",
        Portuguese: "Hipotireoidismo",
        Spanish: "Hipotiroidismo"
    },
    {
        English: "Hyperthyroidism",
        Portuguese: "Hipertireoidismo",
        Spanish: "Hipertiroidismo"
    },
    {
        English: "Other metabolic or endocrine disorder",
        Portuguese: "Outros distúrbios metabólicos ou endócrinos",
        Spanish: "Otro trastorno metabólico o endocrino"
    },
    {
        English: "Gastrointestinal disorders",
        Portuguese: "distúrbio gastrointestinais",
        Spanish: "Trastornos gastrointestinales"
    },
    {
        English: "G.E. Re-flux / Heartburn",
        Portuguese: "G.E. Refluxo / Azia",
        Spanish: "Reflujo G.E. / Acidez"
    },
    {
        English: "Gastric Ulcers / Gastritis",
        Portuguese: "Úlceras Gástricas / Gastrite",
        Spanish: "Úlceras gástricas / Gastritis"
    },
    {
        English: "Inflammatory Bowell Disease",
        Portuguese: "Doença inflamatória intestinal",
        Spanish: "Enfermedad inflamatoria intestinal"
    },
    {
        English: "Other Gastrointestinal disorders",
        Portuguese: "Outros Distúrbios Gastrointestinais",
        Spanish: "Otros trastornos gastrointestinales"
    },
    {
        English: "Bone / Joint disorders",
        Portuguese: "distúrbio ósseo/articular",
        Spanish: "Trastornos óseos/articulares"
    },
    {
        English: "Artificial joint",
        Portuguese: "Articulação artificial",
        Spanish: "Articulación artificial"
    },
    {
        English: "Osteoporosis",
        Portuguese: "Osteoporose",
        Spanish: "Osteoporosis"
    },
    {
        English: "History of Bisphosphonates (including Fosamax, Actonel, Boniva)",
        Portuguese: "História de bifosfonatos (incluindo Fosamax, Actonel, Boniva)",
        Spanish: "Historial de bisfosfonatos (incluyendo Fosamax, Actonel, Boniva)"
    },
    {
        English: "Other Bone / Joint disorders",
        Portuguese: "Outros distúrbios ósseos/articulares",
        Spanish: "Otros trastornos óseos/articulares"
    },
    {
        English: "Respiratory / Lung disorders",
        Portuguese: "distúrbio respiratório/pulmão",
        Spanish: "Trastornos respiratorios/pulmonares"
    },
    {
        English: "Asthma",
        Portuguese: "Asma",
        Spanish: "Asma"
    },
    {
        English: "Emphysema or COPD",
        Portuguese: "Enfisema ou DPOC",
        Spanish: "Enfisema o EPOC"
    },
    {
        English: "Bronchitis",
        Portuguese: "Bronquite",
        Spanish: "Bronquitis"
    },
    {
        English: "Other Respiratory / Lung condition",
        Portuguese: "Outra condição respiratória / pulmonar",
        Spanish: "Otra condición respiratoria/pulmonar"
    },
    {
        English: "Organ disorders",
        Portuguese: "distúrbio de órgãos",
        Spanish: "Trastornos de órganos"
    },
    {
        English: "Kidney Disease",
        Portuguese: "Doença Renal",
        Spanish: "Enfermedad Renal"
    },
    {
        English: "Liver disease",
        Portuguese: "Doença hepática",
        Spanish: "Enfermedad Hepática"
    },
    {
        English: "Eye disease (including glaucoma)",
        Portuguese: "Doença ocular (incluindo glaucoma)",
        Spanish: "Enfermedad ocular (incluyendo glaucoma)"
    },
    {
        English: "Other organ disorder",
        Portuguese: "Outros distúrbios de órgãos ",
        Spanish: "Otro trastorno de órganos"
    },
    {
        English: "Infectious disease",
        Portuguese: "doença infecciosa",
        Spanish: "Enfermedad infecciosa"
    },
    {
        English: "AIDS / HIV",
        Portuguese: "AIDS / HIV",
        Spanish: "SIDA / VIH"
    },
    {
        English: "Hepatitis",
        Portuguese: "Hepatite",
        Spanish: "Hepatitis"
    },
    {
        English: "Other Sexually Transmitted Disease",
        Portuguese: "Outra Doença Sexualmente Transmissível",
        Spanish: "Otra Enfermedad de Transmisión Sexual"
    },
    {
        English: "Tuberculosis",
        Portuguese: "Tuberculose",
        Spanish: "Tuberculosis"
    },
    {
        English: "Other infectious disease",
        Portuguese: "Outra doença infecciosa",
        Spanish: "Otra enfermedad infecciosa"
    },
    {
        English: "Neurological disorders",
        Portuguese: "Distúrbio Neurológico",
        Spanish: "Trastornos Neurológicos"
    },
    {
        English: "Epilepsy",
        Portuguese: "Epilepsia",
        Spanish: "Epilepsia"
    },
    {
        English: "Tourette Syndrome",
        Portuguese: "Síndrome de Tourette",
        Spanish: "Síndrome de Tourette"
    },
    {
        English: "Stroke",
        Portuguese: "Acidente Vascular Cerebral",
        Spanish: "Accidente Cerebrovascular"
    },
    {
        English: "Migraine",
        Portuguese: "Enxaqueca",
        Spanish: "Migraña"
    },
    {
        English: "Other Neurological disorders",
        Portuguese: "Outros distúrbios neurológicos",
        Spanish: "Otros trastornos neurológicos"
    },
    {
        English: "Immune diseases",
        Portuguese: "doença imunológica",
        Spanish: "Enfermedades inmunológicas"
    },
    {
        English: "Lupus",
        Portuguese: "Lúpus",
        Spanish: "Lupus"
    },
    {
        English: "Rheumatoid Arthritis",
        Portuguese: "Artrite Reumatoide",
        Spanish: "Artritis Reumatoide"
    },
    {
        English: "Sjögren’s syndrome",
        Portuguese: "Síndrome de Sjögren",
        Spanish: "Síndrome de Sjögren"
    },
    {
        English: "Myasthenia Gravis",
        Portuguese: "Miastenia Graves",
        Spanish: "Miastenia Gravis"
    },
    {
        English: "Other immune diseases",
        Portuguese: "Outras doenças imunológicas",
        Spanish: "Otras enfermedades inmunológicas"
    },
    {
        English: "Psychotic / Behavioral disorders",
        Portuguese: "distúrbio psicótico/comportamental",
        Spanish: "Trastornos psicóticos/ del comportamiento"
    },
    {
        English: "ADD / ADHD",
        Portuguese: "ADD / TDAH",
        Spanish: "TDAH / ADD"
    },
    {
        English: "Depression",
        Portuguese: "Depressão",
        Spanish: "Depresión"
    },
    {
        English: "Anxiety / Panic Attacks",
        Portuguese: "Ansiedade / Ataques de Pânico",
        Spanish: "Ansiedad / Ataques de Pánico"
    },
    {
        English: "Eating disorder",
        Portuguese: "Transtorno alimentar",
        Spanish: "Trastorno alimenticio"
    },
    {
        English: "Other behavioral disorders",
        Portuguese: "Outros distúrbios comportamentais",
        Spanish: "Otros trastornos del comportamiento"
    },
    {
        English: "Yes, I do have one of those conditions",
        Portuguese: "Sim, eu tenho uma dessas condições",
        Spanish: "Sí, tengo una de esas condiciones"
    },
    {
        English: "No, I don't have any one of those conditions",
        Portuguese: "Não, eu não tenho uma dessas condições",
        Spanish: "No, no tengo ninguna de esas condiciones"
    },
    {
        English: "Do you have any Metabolic / Endocrine disorders?",
        Portuguese: "Você tem algum distúrbio metabólico/endócrino?",
        Spanish: "¿Tiene algún trastorno metabólico/endocrino?"
    }
];



        var inEnglish = <?php echo  ($LANG_ID == 0) ? 'true' : 'false'; ?>;
        var condTypeArray = ["Heart/Blood disorders",
            "Metabolic / Endocrine disorders",
            "Gastrointestinal disorders",
            "Bone / Joint disorders",
            "Respiratory / Lung disorders",
            "Organ disorders",
            "Infectious disease",
            "Neurological disorders",
            "Immune diseases",
            "Psychotic / Behavioral disorders"
        ];
        var condListArray = ({});
        condListArray["Heart/Blood disorders"]=["Artificial Heart Valve(s)",
                        "Congenital Heart Defects",
                        "Heart Murmurs",
                        "Angina",
                        "Congestive Heart Failure",
                        "Heart Surgery",
                        "Pacemaker / defibrillator",
                        "Bacterial Endocarditis",
                        "Coronary Artery Disease",
                        "High Blood Pressure",
                        "Low Blood Pressure",
                        "Abnormal Bleeding",
                        "Methemoglobinemia",
                        "Hemophillia",
                        "Anemia",
                        "Other Heart Conditions"
                    ];
                    
        condListArray["Metabolic / Endocrine disorders"] = ["Diabetes",
                        "Hypothyroidism",
                        "Hyperthyroidism",
                        "Other metabolic or endocrine disorder"
                    ];
        condListArray["Gastrointestinal disorders"] = ["G.E. Re-flux / Heartburn",
                        "Gastric Ulcers / Gastritis",
                        "Inflammatory Bowell Disease",
                        "Other Gastrointestinal disorders"
                    ];
        condListArray["Bone / Joint disorders"] = ["Artificial joint",
                        "Osteoporosis",
                        "History of Bisphosphonates"+
                        " (including Fosamax, Actonel, Boniva)",
                        "Other Bone / Joint disorders"
                    ];
        condListArray["Respiratory / Lung disorders"] = ["Asthma",
                        "Emphysema or COPD",
                        "Bronchitis",
                        "Other Respiratory / Lung condition"
                    ];
        condListArray["Organ disorders"] = ["Kidney Disease",
                        "Liver disease",
                        "Eye disease (including glaucoma)",
                        "Other organ disorder"];
        condListArray["Infectious disease"] = ["AIDS / HIV",
                        "Hepatitis",
                        "Other Sexually Transmitted Disease",
                        "Tuberculosis",
                        "Other infectious disease"
                    ];
        condListArray["Neurological disorders"] = ["Epilepsy",
                        "Stroke",
                        "Migrine",
                        "Other Neurological disorders"
                    ];
        condListArray["Immune diseases"] = ["Lupus",
                        "Rheumatoid Arthritis",
                        "Sjögren’s syndrome",
                        "Myasthenia Gravis",
                        "Other immune diseases"
                    ];
        condListArray["Psychotic / Behavioral disorders"] =  ["ADD / ADHD",
                        "Depression",
                        "Anxiety / Panic Attacks",
                        "Eating disorder",
                        "Other behavioral disorders"
                    ];

        var condIndex =0;
        function handleShowConditons() {
            //first hide the question 
            var hideMe = document.getElementById("condTitle");
            hideMe.style.display = "none";
            hideMe = document.getElementById("condExample");
            hideMe.style.display = "none";
            hideMe = document.getElementById("hasCondOrNotButRow");
            hideMe.style.display = "none";
            
            //Now populate the conditions
            var condName = condTypeArray[condIndex];
            var condList = condListArray[condName];
            var whereToAdd = document.getElementById("allCondRow");
            var setMe = "";
            for(var i=0;i<condList.length;i++) {
                var condStringID = "condition-" + condList[i];
                setMe += "            <div class=\"col-12 mt-2 mb-2\">\n";
                setMe += "                <div class=\"form-check\">\n";
                setMe += "                    <input class=\"form-check-input\" type=\"checkbox\" value=\""+ condStringID  +"\" id=\""+condStringID+"\">\n";
                setMe += "                    <label class=\"form-check-label\" for=\""+condStringID+"\">"+createTranslation(condList[i])+"</label>\n";
                setMe += "                </div>\n";
                setMe += "            </div>\n";
            }
            whereToAdd.innerHTML = setMe;
            
            //Now show the conditions 
            var showMe = document.getElementById("allCondRow");
            showMe.style.display="";
            showMe = document.getElementById("doneButRow");
            showMe.style.display="";
            showMe = document.getElementById("selectCondsHeader");
            showMe.style.display="";
        }


        
        function handleNextCondition() {
            updateAndShowQuestion();
        }
        
        function handleFinishedWithCondition() {
            var docForm = document.forms["MedicalConditions"];
            var condName = condTypeArray[condIndex];
            var condList = condListArray[condName];
            var whereToAdd = document.getElementById("hideInMe");
            for(var i=0;i<condList.length;i++) {
                var condStringID = "condition-" + condList[i];
                var condValue = document.forms["MedicalConditions"][condStringID].checked;
                if(condValue === true) {
                    whereToAdd.innerHTML += "<input name=\""+condStringID+"\" type=\"hidden\" value=\""+condStringID+"\">";
                }
            }
            updateAndShowQuestion();
        }
        
        function updateAndShowQuestion() {
            condIndex++;
            
            if(condIndex === condTypeArray.length) {
                //hide the conditions 
                var hideMe = document.getElementById("allCondRow");
                hideMe.style.display="none";
                hideMe = document.getElementById("doneButRow");
                hideMe.style.display="none";
                hideMe = document.getElementById("selectCondsHeader");
                hideMe.style.display="none";
                hideMe = document.getElementById("condTitle");
                hideMe.style.display = "none";
                hideMe = document.getElementById("condExample");
                hideMe.style.display = "none";
                hideMe = document.getElementById("hasCondOrNotButRow");
                hideMe.style.display = "none";
                
                var showMe = document.getElementById("finishRow");
                showMe.style.display = "";
            
                return;
            }
            
            var condName = condTypeArray[condIndex];
            var condListShow = [];

            if(!inEnglish) {
                for(var i=0;i<condListArray[condName].length;i++) {
                    condListShow.push(createTranslation(condListArray[condName][i]));;
                }
            }
            else {
                condListShow = condListArray[condName];
            }
            
            var condTitle = document.getElementById("condTitle");
            condTitle.innerHTML =  "<?php echo $translationArray[121][$LANG_ID]?>  " + createTranslation(condName) + "?";
            
            var condExample = document.getElementById("condExample");
            condExample.innerHTML = "<?php echo $translationArray[122][$LANG_ID]?>: " + condListShow.join(", ");
        
            //show the question 
            var showMe = document.getElementById("condTitle");
            showMe.style.display = "";
            showMe = document.getElementById("condExample");
            showMe.style.display = "";
            showMe = document.getElementById("hasCondOrNotButRow");
            showMe.style.display = "";
            
            //hide the conditions 
            var hideMe = document.getElementById("allCondRow");
            hideMe.style.display="none";
            hideMe = document.getElementById("doneButRow");
            hideMe.style.display="none";
            hideMe = document.getElementById("selectCondsHeader");
            hideMe.style.display="none";
        }

        function createTranslation(inputPh) {
            if(inEnglish) {
                return inputPh;
            }

            var langKey = "<?php echo ( ($LANG_ID == 1) ? "Portuguese" : "Spanish"); ?>";
            for(var i=0;i<conditionTrArray.length;i++) {
                if(conditionTrArray[i]["English"] == inputPh) {
                    return conditionTrArray[i][langKey];
                }

            }

            return inputPh;

        }

        condIndex = -1;
        updateAndShowQuestion();
  </script>
</form>
<?php printFooter(); ?>
