# Cleardental-NewPatientPortal-Web-Client

PHP Implementation of the new patient form. 

**Requirements**    
* A webserver (Apache or NGINX) that can run PHP 

**How it works**    
This website uses the `$_SESSION` varible to keep track of the user input. Based on the user's input, it may show a different kind of page or use a different translation. At the end, it will do a full datadump as a JSON file to `$SAVE_HASH`.

**How to Set it Up**
1. Copy all files to your webserver's directory. For example, for Apache, it uses `/var/www/html`. However, if you are also hosting another website, you probably would want to put it on its own subdirectory like `/var/www/html/newPatientForm/`.
2. Edit `Configure.php`. For `$SAVE_HASH`, use a random string of at least 12 characters. This is effectively the username / password for the site. Also remember to replace practiceLogo.png with your own practice's logo.
3. Rename the `submitDir` directory to the same `$SAVE_HASH` that you used in the previous step.
4. In Clear.Dental Preferences, go to "Locations" -> Your practice -> Location Information and then enter in the full URL of the website + the `$SAVE_HASH`. For example, lets say the website is https://example.dental/ and you put all the PHP files in to `/var/www/html/newPatientForm/` and `$SAVE_HASH` is `oifg3443dlk32dfljsdfg4kjl`. You would enter in https://example.dental/newPatientForm/oifg3443dlk32dfljsdfg4kjl/
5. Launch the Clear.Dental New Patient module, select "Import Patient from Online", the listing URL should be already pre-filled, and then you can get the new patient list. 