<?php
require('Common.php');
printHeader("Your Dental Plan information");
?>
<form action="MedicalHistory.php" method="POST">

<div class="container mt-3">

    <div class="row" id="askHeader">
            <div class="col-auto mb-5 ">
                <h2><?php echo $translationArray[84][$LANG_ID]?></h2>
            </div>
    </div>
    
    <div class="row justify-content-between" id="askButtons">
            <div class="col-auto">
                <button type="button" class="btn btn-primary btn-lg mb-5" onclick="showDentalPlanQuestions()"><?php echo $translationArray[85][$LANG_ID]?></button>
            </div>
            <div class="col-auto">
                <a class="btn btn-primary btn-lg" href="MedicalHistory.php" role="button"><?php echo $translationArray[86][$LANG_ID]?></a>
            </div>
    </div>
    
    <script>
        function showDentalPlanQuestions() {
            var askHeader = document.getElementById("askHeader");
            var askButtons = document.getElementById("askButtons");
            var dentalPlanInfoDiv = document.getElementById("dentalPlanInfoDiv");
            askHeader.style.display = "none";
            askButtons.style.display = "none";
            dentalPlanInfoDiv.style.display = "";
        }
    </script>
    
    
    <div style="display:none" id="dentalPlanInfoDiv">
        <div class="row">
            <div class="col-auto mb-2 ">
                <h2><?php echo $translationArray[87][$LANG_ID]?></h2>
                <h3><?php echo $translationArray[88][$LANG_ID]?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 mb-4">
                <label for="priInsName" class="form-label"><?php echo $translationArray[89][$LANG_ID]?><span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="priInsName" name="priInsName" placeholder="" value=""  required>
            </div>
            
            <div class="col-12 col-md-6 mb-4">
                <label for="priInsAddr" class="form-label"><?php echo $translationArray[90][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsAddr" name="priInsAddr" placeholder="" value="">
            </div>
            
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsCity" class="form-label"><?php echo $translationArray[91][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsCity" name="priInsCity" placeholder="" value="" >
            </div>
            
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsState" class="form-label"><?php echo $translationArray[92][$LANG_ID]?></label>
                <select class="form-select" id="priInsState" name="priInsState"  >
                    <?php printStates(); ?>
                </select>
            </div>
            
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsZip" class="form-label"><?php echo $translationArray[93][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsZip" name="priInsZip" placeholder="" value="" >
            </div>
            
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsSubID" class="form-label"><?php echo $translationArray[94][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsSubID" name="priInsSubID" placeholder="" value="" >
            </div>
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsGroupID" class="form-label"><?php echo $translationArray[95][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsGroupID" name="priInsGroupID" placeholder="" value="" >
            </div>
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsEmployerName" class="form-label"><?php echo $translationArray[96][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsEmployerName" name="priInsEmployerName" placeholder="" value="" >
            </div>
        </div>
        
        <div class="row align-items-center">
            <div class="col-12 col-md-4 mb-2">
                <h4><?php echo $translationArray[97][$LANG_ID]?></h4>
            </div>
            <div class="col-12 col-md-4 mb-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="selfPolicyHolder" id="yesSelf" value="yesSelf" checked onchange="updateShowPolicyHolder()">
                    <label class="form-check-label" for="yesSelf"><?php echo $translationArray[98][$LANG_ID]?></label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="selfPolicyHolder" id="noSelf" value="noSelf" onchange="updateShowPolicyHolder()">
                    <label class="form-check-label" for="noSelf"><?php echo $translationArray[99][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <script>
            function updateShowPolicyHolder() {
                var selfPolicyHolder = document.querySelector('input[name="selfPolicyHolder"]:checked'); 
                var primInsPolicyHolderInfo = document.getElementById("primInsPolicyHolderInfo");
                if(selfPolicyHolder.value === "yesSelf") {
                    primInsPolicyHolderInfo.style.display = "none";
                }
                else {
                    primInsPolicyHolderInfo.style.display = "";
                }
            
            }
        </script>
        
        
        <div class="row mt-4" style="display:none" id="primInsPolicyHolderInfo">
            <div class="col-12 mb-2 ">
                <h2><?php echo $translationArray[100][$LANG_ID]?></h2>
            </div>
            
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsPolHolderFirst" class="form-label"><?php echo $translationArray[101][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsPolHolderFirst" name="priInsPolHolderFirst" value="">
            </div>
            
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsPolHolderMiddle" class="form-label"><?php echo $translationArray[102][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsPolHolderMiddle" name="priInsPolHolderMiddle" value="">
            </div>
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsPolHolderLast" class="form-label"><?php echo $translationArray[103][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsPolHolderLast" name="priInsPolHolderLast" value="">
            </div>
            <div class="col-12 mb-4">
                <label for="priInsPolHolderAddress" class="form-label"><?php echo $translationArray[104][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsPolHolderAddress" name="priInsPolHolderAddress" value="">
            </div>
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsPolHolderCity" class="form-label"><?php echo $translationArray[105][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsPolHolderCity" name="priInsPolHolderCity" value="">
            </div>
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsPolHolderState" class="form-label"><?php echo $translationArray[106][$LANG_ID]?></label>
                <select class="form-select" id="priInsPolHolderState" name="priInsPolHolderState" >
                    <?php printStates(); ?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsPolHolderZip" class="form-label"><?php echo $translationArray[107][$LANG_ID]?></label>
                <input type="text" class="form-control" id="priInsPolHolderZip" name="priInsPolHolderZip" value="">
            </div>
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsPolHolderDOB" class="form-label"><?php echo $translationArray[108][$LANG_ID]?></label>
                <input type="date" class="form-control" id="priInsPolHolderDOB" name="priInsPolHolderDOB">
            </div>
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsPolHolderGender" class="form-label"><?php echo $translationArray[109][$LANG_ID]?></label>
                <select class="form-select" id="priInsPolHolderGender" name="priInsPolHolderGender" >
                    <option value="Male"><?php echo $translationArray[38][$LANG_ID]?></option>
                    <option value="Female"><?php echo $translationArray[39][$LANG_ID]?></option>
                    <option value="Other"><?php echo $translationArray[40][$LANG_ID]?></option>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-4">
                <label for="priInsPolHolderRel" class="form-label"><?php echo $translationArray[110][$LANG_ID]?></label>
                <select class="form-select" id="priInsPolHolderRel" name="priInsPolHolderRel" >
                    <option value="Spouse">Spouse</option>
                    <option value="Dependent Child">Dependent Child</option>
                    <option value="Other">Other</option>
                </select>
            </div>
        </div> <!--End of primInsPolicyHolderInfo-->
        
        
        <div class="row align-items-center">
            <div class="col-12 col-md-5 mb-2">
                <h4><?php echo $translationArray[111][$LANG_ID]?></h2>
            </div>
            <div class="col-12 col-md-4 mb-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="hasSecondary" id="yesHasSecondary" value="yesHasSecondary" onchange="updateShowSecondary()" >
                    <label class="form-check-label" for="yesHasSecondary">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="hasSecondary" id="noHasSecondary" value="noHasSecondary" onchange="updateShowSecondary()" checked>
                    <label class="form-check-label" for="noHasSecondary">No</label>
                </div>
            </div>
        </div>
        
        <script>
        
            function updateShowSecondary() {
                var hasSecondary = document.querySelector('input[name="hasSecondary"]:checked'); 
                var secondaryInsInfo = document.getElementById("secondaryInsInfo");
                if(hasSecondary.value === "yesHasSecondary") {
                    secondaryInsInfo.style.display = "";
                }
                else {
                    secondaryInsInfo.style.display = "none";
                }
            
            }
        </script>
        
        <div class="row mt-4" id="secondaryInsInfo" style="display:none">
            <div class="row">
                <div class="col-12 mb-2 ">
                    <h2><?php echo $translationArray[112][$LANG_ID]?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 mb-2 ">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="secInsMedical" value="secInsMedical" id="secInsMedical">
                        <label class="form-check-label" for="secInsMedical"><?php echo $translationArray[113][$LANG_ID]?></label>
                    </div>
                </div>
                <div class="col-12 col-md-6 mb-2 ">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="secInsDental" value="secInsDental" id="secInsDental">
                        <label class="form-check-label" for="secInsDental"><?php echo $translationArray[114][$LANG_ID]?></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 mb-4">
                    <label for="secInsName" class="form-label">Secondary Plan Company Name</label>
                    <input type="text" class="form-control" id="secInsName" name="secInsName" value="">
                </div>
                
                <div class="col-12 col-md-6 mb-4">
                    <label for="secInsAddr" class="form-label">Secondary Plan Company Address</label>
                    <input type="text" class="form-control" id="secInsAddr" name="secInsAddr" value="">
                </div>
                
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsCity" class="form-label">Secondary Plan Company City</label>
                    <input type="text" class="form-control" id="secInsCity" name="secInsCity" value="">
                </div>
                
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsState" class="form-label">Secondary Plan Company State</label>
                    <select class="form-select" id="secInsState" name="secInsState" >
                        <?php printStates(); ?>
                    </select>
                </div>
                
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsZip" class="form-label">Secondary Plan Company Zip</label>
                    <input type="text" class="form-control" id="secInsZip" name="secInsZip" value="">
                </div>
                
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsSubID" class="form-label">Secondary Policyholder/Subscriber ID</label>
                    <input type="text" class="form-control" id="secInsSubID" name="secInsSubID" value="">
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsGroupID" class="form-label">Secondary Plan/Group Number</label>
                    <input type="text" class="form-control" id="secInsGroupID" name="secInsGroupID" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 mb-2">
                    <h4>Are you the policyholder of this secondary plan?</h2>
                </div>
                <div class="col-12 col-md-4 mb-2">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="isPolHolderSecondary" id="yesPolHolderSecondary" value="yesPolHolderSecondary" onchange="updateShowSecondaryPolHolder()" checked >
                        <label class="form-check-label" for="yesPolHolderSecondary">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="isPolHolderSecondary" id="noPolHolderSecondary" value="noPolHolderSecondary" onchange="updateShowSecondaryPolHolder()">
                        <label class="form-check-label" for="noPolHolderSecondary">No</label>
                    </div>
                </div>
            </div>
            
            <script>
                function updateShowSecondaryPolHolder() {
                    var isPolHolderSecondary = document.querySelector('input[name="isPolHolderSecondary"]:checked'); 
                    var secInsPolicyHolderInfo = document.getElementById("secInsPolicyHolderInfo");
                    if(isPolHolderSecondary.value === "yesPolHolderSecondary") {
                        secInsPolicyHolderInfo.style.display = "none";
                    }
                    else {
                        secInsPolicyHolderInfo.style.display = "";
                    }
                }
            </script>
            
            <div class="row mt-4" style="display:none" id="secInsPolicyHolderInfo">
                <div class="col-12 mb-2 ">
                    <h4>Please give the information about the policyholder of this secondary plan</h4>
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsPolHolderFirst" class="form-label">Secondary Policyholder First Name</label>
                    <input type="text" class="form-control" id="secInsPolHolderFirst" name="secInsPolHolderFirst" value="">
                </div>
                
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsPolHolderMiddle" class="form-label">Secondary Policyholder Middle Name</label>
                    <input type="text" class="form-control" id="secInsPolHolderMiddle" name="secInsPolHolderMiddle" value="">
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsPolHolderLast" class="form-label">Secondary Policyholder Last Name</label>
                    <input type="text" class="form-control" id="secInsPolHolderLast" name="secInsPolHolderLast" value="">
                </div>
                <div class="col-12 mb-4">
                    <label for="secInsPolHolderLast" class="form-label">Secondary Policyholder Address</label>
                    <input type="text" class="form-control" id="secInsPolHolderLast" name="secInsPolHolderLast" value="">
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsPolHolderCity" class="form-label">Secondary Policyholder City</label>
                    <input type="text" class="form-control" id="secInsPolHolderCity" name="secInsPolHolderCity" value="">
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsPolHolderState" class="form-label">Secondary Policyholder State</label>
                    <select class="form-select" id="secInsPolHolderState" name="secInsPolHolderState" >
                        <?php printStates(); ?>
                    </select>
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsPolHolderZip" class="form-label">Secondary Policyholder Zip</label>
                    <input type="text" class="form-control" id="secInsPolHolderZip" name="secInsPolHolderZip" value="">
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsPolHolderDOB" class="form-label">Secondary Policyholder Date of Birth</label>
                    <input type="date" class="form-control" id="secInsPolHolderDOB" name="secInsPolHolderDOB">
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsPolHolderGender" class="form-label">Secondary Policyholder Gender</label>
                    <select class="form-select" id="secInsPolHolderGender" name="secInsPolHolderGender" >
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <label for="secInsPolHolderRel" class="form-label">Relationship to Secondary Policyholder</label>
                    <select class="form-select" id="secInsPolHolderRel" name="secInsPolHolderRel">
                        <option value="Spouse">Spouse</option>
                        <option value="Dependent Child">Dependent Child</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
            </div>
        </div>
    
        
        <div class="row justify-content-end my-3 mx-1">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
            </div>
        </div>
    </div>
    
    
    
</div>
</form>
<?php printFooter(); ?>
