<?php
require('Common.php');
printHeader("Finished!");

$_SESSION["submitTime"] = time();
foreach ($_POST as $key => $value) {
    $_SESSION[$key] = $value;
}

$firstName = trim($_SESSION["firstName"]);
$lastName = trim($_SESSION["lastName"]);
$_SESSION["dob"] = $_SESSION["dob-year"]."-".$_SESSION["dob-month"]."-".$_SESSION["dob-day"];
$dob =  DateTime::createFromFormat("Y-m-d",$_SESSION["dob"]);
$dobString = date_format($dob,"M-j-Y");

$saveFile = fopen($SAVE_HASH."/".$lastName.", ".$firstName." (".$dobString.").json","w");
fwrite($saveFile, json_encode($_SESSION, JSON_PRETTY_PRINT));
fclose($saveFile);
?>

<div class="container mt-3">
    <div class="row p-3 justify-content-center">
        <div class="col-auto">
            <h2><?php echo $translationArray[213][$LANG_ID]?></h2>
        </div>
    </div>
        
    <div class="row p-3 justify-content-center">
        <div class="col-auto">
            <p><?php echo $translationArray[214][$LANG_ID]?></p>
        </div>
    </div>
    <div class="row p-3 justify-content-center">
        <div class="col-auto">
            <a class="btn btn-primary btn-lg mb-3" href="<?php echo $PRACTICE_WEBSITE?>" role="button">Back to <?php echo $PRACTICE_NAME?> Website</a>
        </div>
    </div>
</div>

<?php printFooter(); ?>
