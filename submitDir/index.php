<?php
$fakeResults = scandir(".");
$results = array();
foreach($fakeResults as $file) {
    if(strpos($file,".json") > 0) {
        $results[$file] = filemtime($file);
    }
}

arsort($results);

echo json_encode($results, JSON_PRETTY_PRINT);

?>
