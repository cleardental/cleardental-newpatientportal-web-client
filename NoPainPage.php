<?php
require('Common.php');
printHeader("Your dental History");
$_SESSION["patientInPain"] = false;
?>
<form action="BasicPersonalInfoPage.php" method="POST">
    <div class="container mt-3">
        <div class="row justify-content-center mb-2">
            <div class="col-auto">
                <h2><?php echo $translationArray[215][$LANG_ID]?></h2>
            </div>
        </div>
        
        <div class="row mb-2">
            <div class="col-auto">
                <h3><?php echo $translationArray[216][$LANG_ID]?></h3>
            </div>
        </div>
        
        
        <div class="row ">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[217][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="cc" id="ccNone" value="ccNone" onchange="updateCC()" required>
                    <label class="form-check-label" for="ccNone"><?php echo $translationArray[218][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="cc" id="ccSen" value="ccSen" onchange="updateCC()" required>
                    <label class="form-check-label" for="ccSen"><?php echo $translationArray[219][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="cc" id="ccLooks" value="ccLooks" onchange="updateCC()" required>
                    <label class="form-check-label" for="ccLooks"><?php echo $translationArray[220][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="cc" id="ccChip" value="ccChip" onchange="updateCC()" required>
                    <label class="form-check-label" for="ccChip"><?php echo $translationArray[221][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row mt-2">
            <div class="col-auto" id="whereSenCol" style="display:none">
                <label for="whereSen" class="form-label"><?php echo $translationArray[222][$LANG_ID]?></label>
                <input type="text" class="form-control" name="whereSen" id="whereSen">
            </div>
            <div class="col-auto" id="whereChipCol" style="display:none">
                <label for="whereChip" class="form-label"><?php echo $translationArray[223][$LANG_ID]?></label>
                <input type="text" class="form-control" name="whereChip" id="whereChip">
            </div>
        </div>
        
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[224][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="getDone" id="justClean" value="justClean" required>
                    <label class="form-check-label" for="justClean"><?php echo $translationArray[225][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="getDone" id="needFilling" value="needFilling" required>
                    <label class="form-check-label" for="needFilling"><?php echo $translationArray[226][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="getDone" id="wantLookBetter" value="wantLookBetter" required>
                    <label class="form-check-label" for="wantLookBetter"><?php echo $translationArray[227][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="getDone" id="dontKnowGetDone" value="dontKnowGetDone"  required>
                    <label class="form-check-label" for="dontKnowGetDone"><?php echo $translationArray[228][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row mt-2 ">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[229][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="docIssue" id="docIssueNone" value="docIssueNone" required>
                    <label class="form-check-label" for="docIssueNone"><?php echo $translationArray[230][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="docIssue" id="noLike" value="noLike" required>
                    <label class="form-check-label" for="noLike"><?php echo $translationArray[231][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="docIssue" id="chairIssue" value="chairIssue"  required>
                    <label class="form-check-label" for="chairIssue"><?php echo $translationArray[232][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="docIssue" id="scared" value="scared" required>
                    <label class="form-check-label" for="scared"><?php echo $translationArray[233][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[234][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="lastCleaning" id="6mon" value="6mon" required>
                    <label class="form-check-label" for="6mon"><?php echo $translationArray[235][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="lastCleaning" id="shortClean" value="shortClean" required>
                    <label class="form-check-label" for="shortClean"><?php echo $translationArray[236][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="lastCleaning" id="moreYear" value="moreYear"  required>
                    <label class="form-check-label" for="moreYear"><?php echo $translationArray[237][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="lastCleaning" id="moreYear2" value="moreYear2" required>
                    <label class="form-check-label" for="moreYear2"><?php echo $translationArray[238][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="lastCleaning" id="manyYears" value="manyYears" required>
                    <label class="form-check-label" for="manyYears"><?php echo $translationArray[239][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[240][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hadSCRP" id="scrpRecent" value="scrpRecent" required>
                    <label class="form-check-label" for="scrpRecent"><?php echo $translationArray[241][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hadSCRP" id="scrpLong" value="scrpLong" required>
                    <label class="form-check-label" for="scrpLong"><?php echo $translationArray[242][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hadSCRP" id="scrpNever" value="scrpNever"  required>
                    <label class="form-check-label" for="scrpNever"><?php echo $translationArray[243][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hadSCRP" id="scrpNoClue" value="scrpNoClue" required>
                    <label class="form-check-label" for="scrpNoClue"><?php echo $translationArray[244][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[245][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="oftenBrush" id="brushTwice" value="brushTwice" required>
                    <label class="form-check-label" for="brushTwice"><?php echo $translationArray[246][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="oftenBrush" id="brushOnce" value="brushOnce" required>
                    <label class="form-check-label" for="brushOnce"><?php echo $translationArray[247][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="oftenBrush" id="brushWeekly" value="brushWeekly"  required>
                    <label class="form-check-label" for="brushWeekly"><?php echo $translationArray[248][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="oftenBrush" id="brushNever" value="brushNever" required>
                    <label class="form-check-label" for="brushNever"><?php echo $translationArray[249][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[250][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="oftenFloss" id="flossTwice" value="flossTwice" required>
                    <label class="form-check-label" for="flossTwice"><?php echo $translationArray[246][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="oftenFloss" id="flossOnce" value="flossOnce" required>
                    <label class="form-check-label" for="flossOnce"><?php echo $translationArray[247][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="oftenFloss" id="flossWeekly" value="flossWeekly"  required>
                    <label class="form-check-label" for="flossWeekly"><?php echo $translationArray[248][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="oftenFloss" id="flossNever" value="flossNever" required>
                    <label class="form-check-label" for="flossNever"><?php echo $translationArray[249][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[251][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gumBleed" id="gumBleedNever" value="gumBleedNever" required>
                    <label class="form-check-label" for="gumBleedNever"><?php echo $translationArray[252][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gumBleed" id="gumBleedSometimes" value="gumBleedSometimes" required>
                    <label class="form-check-label" for="gumBleedSometimes"><?php echo $translationArray[253][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gumBleed" id="gumBleedRare" value="gumBleedRare"  required>
                    <label class="form-check-label" for="gumBleedRare"><?php echo $translationArray[254][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gumBleed" id="gumBleedAllTime" value="gumBleedAllTime" required>
                    <label class="form-check-label" for="gumBleedAllTime"><?php echo $translationArray[255][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[256][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="haveHeadaches" id="neverHead" value="neverHead" required>
                    <label class="form-check-label" for="neverHead"><?php echo $translationArray[257][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="haveHeadaches" id="someHead" value="someHead" required>
                    <label class="form-check-label" for="someHead"><?php echo $translationArray[258][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="haveHeadaches" id="allHeadaches" value="allHeadaches"  required>
                    <label class="form-check-label" for="allHeadaches"><?php echo $translationArray[259][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[260][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="smoke" id="neverSmoke" value="neverSmoke" required>
                    <label class="form-check-label" for="neverSmoke"><?php echo $translationArray[261][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="smoke" id="usedToSmoke" value="usedToSmoke" required>
                    <label class="form-check-label" for="usedToSmoke"><?php echo $translationArray[262][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="smoke" id="smokeRare" value="smokeRare" required>
                    <label class="form-check-label" for="smokeRare"><?php echo $translationArray[263][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="smoke" id="smokeDaily" value="smokeDaily" required>
                    <label class="form-check-label" for="smokeDaily"><?php echo $translationArray[264][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[265][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="nightGrind" id="neverGrind" value="neverGrind" required>
                    <label class="form-check-label" for="neverGrind"><?php echo $translationArray[266][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="nightGrind" id="noClueGrind" value="noClueGrind" required>
                    <label class="form-check-label" for="noClueGrind"><?php echo $translationArray[267][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="nightGrind" id="sometimesGrind" value="sometimesGrind" required>
                    <label class="form-check-label" for="sometimesGrind"><?php echo $translationArray[268][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="nightGrind" id="usedToGrind" value="usedToGrind" required>
                    <label class="form-check-label" for="usedToGrind"><?php echo $translationArray[269][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="nightGrind" id="grindAll" value="grindAll"  required>
                    <label class="form-check-label" for="grindAll"><?php echo $translationArray[270][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[271][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="bracesBefore" id="neverBraces" value="neverBraces" required>
                    <label class="form-check-label" for="neverBraces"><?php echo $translationArray[272][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="bracesBefore" id="yesBracesKid" value="yesBracesKid" required>
                    <label class="form-check-label" for="yesBracesKid"><?php echo $translationArray[273][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="bracesBefore" id="yesBracesRecent" value="yesBracesRecent" required>
                    <label class="form-check-label" for="yesBracesRecent"><?php echo $translationArray[274][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[275][$LANG_ID]?><span class="text-danger">*</span></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="sleepIssue" id="noIssueSleeping" value="noIssueSleeping" required>
                    <label class="form-check-label" for="noIssueSleeping"><?php echo $translationArray[276][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="sleepIssue" id="yesWakeUp" value="yesWakeUp" required>
                    <label class="form-check-label" for="yesWakeUp"><?php echo $translationArray[277][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="sleepIssue" id="yesApnea" value="yesApnea" required>
                    <label class="form-check-label" for="yesApnea"><?php echo $translationArray[278][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="sleepIssue" id="yesOtherSleep" value="yesOtherSleep" required>
                    <label class="form-check-label" for="yesOtherSleep"><?php echo $translationArray[279][$LANG_ID]?></label>
                </div>
            </div>
        </div>
        
        <div class="row justify-content-end my-3 mx-5">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
            </div>
        </div>
       
    
    </div>
</form>
  
    <script>
        function updateCC() {
            var whereSenCol = document.getElementById("whereSenCol");
            var whereChipCol = document.getElementById("whereChipCol");
            
            var checkRadio = document.querySelector('input[name="cc"]:checked').value; 
            if(checkRadio === "ccSen") {
                whereSenCol.style.display="";
                whereChipCol.style.display="none";
            }
            else if(checkRadio === "ccChip") {
                whereSenCol.style.display="none";
                whereChipCol.style.display="";
            }
            else {
                whereSenCol.style.display="none";
                whereChipCol.style.display="none";
            }
        }
    </script>

<?php printFooter(); ?>
