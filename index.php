<?php
require('Common.php');
printHeader($PRACTICE_NAME.': Select Langauge');
?>
<section>
    <div class="container mt-3">
        <div class="row justify-content-center ">
            <div class="col-auto">
                <h1>Please select your language</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-auto">
                <h1>Por favor selecione seu idioma</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-auto">
                <h1>Por favor seleccione su idioma</h1>
            </div>
        </div>
    </div>
</section>

<form action="Start.php" method="POST">
    <section>
        <div class="container">
            <div class="row justify-content-center ">
                <div class="col-auto my-3">
                    <button class="btn btn-primary btn-lg" type="submit" name="language" value="en">
                        <img src="flags/Flag_of_the_United_States.svg" height=16>
                        <img src="flags/Flag_of_the_United_Kingdom.svg" height=16>
                        English
                    </button>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-auto my-3">
                    <button class="btn btn-primary btn-lg"  type="submit"  name="language" value="pr">
                        <img src="flags/Flag_of_Brazil.svg" height=16>
                        <img src="flags/Flag_of_Portugal.svg" height=16>
                        Português
                    </button>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-auto my-3">
                    <button class="btn btn-primary btn-lg"  type="submit"  name="language" value="es">
                        <img src="flags/Bandera_de_España.svg" height=16>
                        Español
                    </button>
                </div>
            </div>
        </div>
    </section>
</form>
<?php printFooter(); ?>
