<?php
require('Common.php');
printHeader("Your Contact Information");
?>
<form action="DentalPlanInfo.php" method="POST">
    <div class="container mt-3">
        <div class="row">
            <div class="col">
                <h2><?php echo $translationArray[280][$LANG_ID]?></h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12 col-md-4 mt-3">
                <div class="form-floating my-1">
                    <input type="text" class="form-control" id="homePhone" name="homePhone" placeholder="" value="">
                    <label for="homePhone" class="form-label"><?php echo $translationArray[281][$LANG_ID]?></label>
                </div>
            </div>
            <div class="col-12 col-md-4 mt-3">
                <div class="form-floating my-1">
                    <input type="text" class="form-control" id="cellPhone" name="cellPhone" placeholder="" value="" required>
                    <label for="cellPhone" class="form-label"><?php echo $translationArray[282][$LANG_ID]?><span class="text-danger">*</span></label>
                </div>
            </div>
            <div class="col-12 col-md-4 mt-3">
                <div class="form-floating my-1">
                    <input type="text" class="form-control" id="workPhone" name="workPhone" placeholder="" value="">
                    <label for="workPhone" class="form-label"><?php echo $translationArray[283][$LANG_ID]?></label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-6 mt-3">
                <div class="form-floating my-1">
                    <input type="text" class="form-control" id="homeEmail" name="homeEmail" placeholder="" value="">
                    <label for="homeEmail" class="form-label"><?php echo $translationArray[284][$LANG_ID]?></label>
                </div>
            </div>
            <div class="col-12 col-md-6 mt-3">
                <div class="form-floating my-1">
                    <input type="text" class="form-control" id="workEmail" name="workEmail" placeholder="" value="">
                    <label for="workEmail" class="form-label"><?php echo $translationArray[285][$LANG_ID]?></label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-4 mt-3">
                <div class="form-floating my-1">
                    <input type="text" class="form-control" id="facebookName" name="facebookName" placeholder="" value="">
                    <label for="facebookName" class="form-label"><?php echo $translationArray[286][$LANG_ID]?></label>
                </div>
            </div>
            <div class="col-12 col-md-4 mt-3">
                <div class="form-floating my-1">
                    <input type="text" class="form-control" id="instagramName" name="instagramName" placeholder="" value="">
                    <label for="instagramName" class="form-label"><?php echo $translationArray[287][$LANG_ID]?></label>
                </div>
            </div>
            <div class="col-12 col-md-4 mt-3">
                <div class="form-floating my-1">
                    <input type="text" class="form-control" id="whatsAppName" name="whatsAppName" placeholder="" value="">
                    <label for="whatsAppName" class="form-label"><?php echo $translationArray[288][$LANG_ID]?></label>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[289][$LANG_ID]?></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="howContact" id="callHome" value="callHome" required>
                    <label class="form-check-label" for="callHome"><?php echo $translationArray[290][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="howContact" id="callCell" value="callCell" required>
                    <label class="form-check-label" for="callCell"><?php echo $translationArray[291][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="howContact" id="callWork" value="callWork" required>
                    <label class="form-check-label" for="callWork"><?php echo $translationArray[292][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="howContact" id="textCell" value="textCell" required>
                    <label class="form-check-label" for="textCell"><?php echo $translationArray[293][$LANG_ID]?>r</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="howContact" id="emailPersonalAddr" value="emailPersonalAddr" required>
                    <label class="form-check-label" for="emailPersonalAddr"><?php echo $translationArray[294][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="howContact" id="mailWorkAddr" value="mailWorkAddr"  required>
                    <label class="form-check-label" for="mailWorkAddr"><?php echo $translationArray[295][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="howContact" id="sendFacebook" value="sendFacebook"  required>
                    <label class="form-check-label" for="sendFacebook"><?php echo $translationArray[296][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="howContact" id="sendInstagram" value="sendInstagram"  required>
                    <label class="form-check-label" for="sendInstagram"><?php echo $translationArray[297][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="howContact" id="sendWhatsApp" value="sendWhatsApp"  required>
                    <label class="form-check-label" for="sendWhatsApp"><?php echo $translationArray[298][$LANG_ID]?></label>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[299][$LANG_ID]?></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="whenAvailable[]" id="availSun" value="Su" >
                    <label class="form-check-label" for="availSun"><?php echo $translationArray[300][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="whenAvailable[]" id="availMon" value="M">
                    <label class="form-check-label" for="availMon"><?php echo $translationArray[301][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="whenAvailable[]" id="availTues" value="Tu">
                    <label class="form-check-label" for="availTues"><?php echo $translationArray[302][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="whenAvailable[]" id="availWeds" value="W">
                    <label class="form-check-label" for="availWeds"><?php echo $translationArray[303][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="whenAvailable[]" id="availThurs" value="Th">
                    <label class="form-check-label" for="availThurs"><?php echo $translationArray[304][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="whenAvailable[]" id="availFris" value="F">
                    <label class="form-check-label" for="availFris"><?php echo $translationArray[305][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="whenAvailable[]" id="availSats" value="Sa">
                    <label class="form-check-label" for="availSats"><?php echo $translationArray[306][$LANG_ID]?></label>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-12 col-md-6">
                <h4><?php echo $translationArray[307][$LANG_ID]?></h4>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="dayTimeAvail[]" id="availEMorning" value="EMorning" >
                    <label class="form-check-label" for="availEMorning"><?php echo $translationArray[308][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="dayTimeAvail[]" id="availMorning" value="Morning">
                    <label class="form-check-label" for="availMorning"><?php echo $translationArray[309][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="dayTimeAvail[]" id="availEAfter" value="EAfter">
                    <label class="form-check-label" for="availEAfter"><?php echo $translationArray[310][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="dayTimeAvail[]" id="availLAfter" value="LAfter">
                    <label class="form-check-label" for="availLAfter"><?php echo $translationArray[311][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="dayTimeAvail[]" id="availEve" value="Eve">
                    <label class="form-check-label" for="availEve"><?php echo $translationArray[312][$LANG_ID]?></label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="dayTimeAvail[]" id="availLEve" value="LEve">
                    <label class="form-check-label" for="availLEve"><?php echo $translationArray[313][$LANG_ID]?></label>
                </div>
            </div>
        </div>

        <div class="row justify-content-end my-3 mx-1">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary btn-lg" id="submitButton"><?php echo $translationArray[30][$LANG_ID]?></button>
            </div>
        </div>
    </div>
</form>
<?php
printFooter();
?>
